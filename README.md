# OM APPARAT #

APPARAT är en Android-applikation som är utvecklad, programmerad och testad med stöd från Internetfonden. Appen registrerar och samlar in data om hur vi använder våra smartphones – direkt från användaren. APPARAT ska öka exaktheten och underlätta undersökningar om mobilenhetsbeteenden.

Appen är tänkt att användas för undersökningar i Sverige inom området smartphoneanvändande görs till stor del med hjälp av telefon- och enkätundersökningar. Med hjälp av denna applikation som kan samla in användardata och användarmönster direkt från smartphones kan vi skapa oss en mer fullständig bild av hur vi använder våra smartphones idag. 

Applikationen utformas för att kartlägga vad vi gör på våra smartphones, hur ofta, hur länge, när och var.
Med hjälp av att kombinera appen med Moves API kan vi analysera var och i vilken hastighet olika applikationer används och utläsa smartphonebeteenden i bilar och i kollektivtrafik. Data samlas först lokalt i smartphonen och delas sedan var tolfte timme till en extern databas.

Koden får användas enligt: Creative Commons-Erkännande-Dela lika (CC-BYSA-3.0).

### För mer information ###

Kontakta Jesper Wachtmeister - jesper@solarisfilm.se