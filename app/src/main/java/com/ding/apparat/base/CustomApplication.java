package com.ding.apparat.base;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Global context for the application. Registers a static instance of itself when application
 * starts, as to provide access to a context for the rest of the application.
 */
public class CustomApplication extends Application {

    /**
     * The globally accessible application context.
     */
    public static CustomApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
    }
}
