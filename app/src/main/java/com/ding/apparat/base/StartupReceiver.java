package com.ding.apparat.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.ding.apparat.gui.UsageSettingActivity;
import com.ding.apparat.monitoring.application.UsageStats;

/**
 * One possible entry point into the application. Broadcast receiver for startup broadcasts.
 */
public class StartupReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // If the user is on Lollipop or higher and usage access is disabled, redirect to settings.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !UsageStats.enabled()) {
            context.startActivity(new Intent(context, UsageSettingActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            return;
        }

        MonitorService.start();
    }
}
