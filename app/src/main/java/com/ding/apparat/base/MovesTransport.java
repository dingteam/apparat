package com.ding.apparat.base;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MovesTransport {
    private static final String TAG = MovesTransport.class.getSimpleName();
    private int id;
    private String activity;
    private String group;
    private Date startTime;
    private Date endTime;
    private boolean edited;

    public MovesTransport(int id, String activity, String group, Date startTime, Date endTime, boolean edited){
        this.id = id;
        this.activity = activity;
        this.group = group;
        this.startTime = startTime;
        this.endTime = endTime;
        this.edited = edited;
    }

    public MovesTransport(){
        this(0, "transport", "transport", new Date(), new Date(), true);
    }

    public MovesTransport(Date startTime, Date endTime){
        this(0, "transport", "transport", startTime, endTime, true);
    }

    public MovesTransport(JSONObject jsonTransport) throws JSONException, ParseException{
        this.id = jsonTransport.getInt("id");
        this.activity = jsonTransport.getString("activity");
        this.group = jsonTransport.getString("group");

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        calendar.setTime(sdf.parse(jsonTransport.getString("startTime")));
        this.startTime = calendar.getTime();

        calendar.setTime(sdf.parse(jsonTransport.getString("endTime")));
        this.endTime = calendar.getTime();

        this.edited = jsonTransport.getInt("edited") == 1;
    }

    @Override
    public String toString() {
        return "MovesTransport{" +
                "id=" + id +
                ", activity='" + activity + '\'' +
                ", group='" + group + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", edited=" + edited +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }
}
