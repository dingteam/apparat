package com.ding.apparat.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.ding.apparat.authentication.User;
import com.ding.apparat.gui.login.LoginActivity;
import com.ding.apparat.gui.menu.MenuActivity;

/**
 * One possible entry point into the application (via launcher click).
 */
public class StartupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (User.loggedIn()) {
            startActivity(new Intent(this, MenuActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }

        finish();
    }
}
