package com.ding.apparat.base;

    /**
     * A custom spinner item that stores two values: One that depends on locale and the other
     * independent of locale.
     */
public class SpinnerItem {

    /**
     * Locale dependent
     */
    public final String view;

    /**
     * Locale independent
     */
    public final String value;

    /**
     * Create a new instance of this class.
     */
    SpinnerItem(String view, String value) {
        this.view = view;
        this.value = value;
    }

    /**
     * Convenience method for creating an array of SpinnerItems. Note that views and values must
     * have corresponding items at corresponding indexes.
     *
     * @param views  The locale dependent strings
     * @param values The locale independent strings
     * @return An array of SpinnerItem with specified views and values.
     */
    public static SpinnerItem[] createFromResource(String[] views, String[] values) {
        SpinnerItem[] result = new SpinnerItem[views.length];

        for (int i = 0; i < result.length; ++i) {
            result[i] = new SpinnerItem(views[i], values[i]);
        }

        return result;
    }

    @Override
    public String toString() {
        return view;
    }
}
