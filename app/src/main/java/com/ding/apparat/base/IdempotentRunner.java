package com.ding.apparat.base;

/**
 * This abstract class can be extended by classes who requires idempotent start and stop operations
 * (that is, any additional calls to start() or stop() after the initial call will not have any
 * effect).
 */
public abstract class IdempotentRunner {

    /**
     * If the runner is currently running.
     */
    private boolean isRunning;

    /**
     * Start the main operation. If it is already started, do nothing.
     */
    public void start() {
        if (isRunning) {
            return;
        }

        isRunning = true;

        startAction();
    }

    /**
     * The action that is to be executed upon starting.
     */
    protected abstract void startAction();

    /**
     * Stop the main operation. If it is already stopped, do nothing.
     */
    public void stop() {
        if (!isRunning) {
            return;
        }

        stopAction();

        isRunning = false;
    }

    /**
     * The action that is to be executed upon stopping.
     */
    protected abstract void stopAction();
}
