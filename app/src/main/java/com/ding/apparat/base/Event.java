package com.ding.apparat.base;

import android.support.annotation.NonNull;
import android.util.Log;

import com.ding.apparat.util.TimeUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class represents an event in this application. It uses reflection to provide functionality
 * for generating json representations of itself and creating new Events from json strings.
 */
public class Event implements Comparable<Event> {

    /**
     * JSON values.
     */
    public static final String TYPE_APPLICATION = "application";

    public static final String TYPE_SMS = "sms";

    public static final String TYPE_MMS = "mms";

    public static final String TYPE_PHONE = "phone";

    public static final String TYPE_CONNECTION = "connection";

    public static final String TYPE_SCREEN = "screen";

    public static final String TYPE_MUSIC = "music";

    public static final String TYPE_OUTGOING = "outgoing";

    public static final String TYPE_INCOMING = "incoming";

    private static final String TAG = Event.class.getSimpleName();

    /**
     * Event data. This data is used with reflection.
     */
    public String type;

    public String application_id;

    public String label;

    public String utc_start;

    public String utc_end;

    public String lon;

    public String lat;

    public String activity;

    /**
     * Default constructor, only used internally.
     */
    private Event() {
    }

    /**
     * Constructor initializing all fields. If utc_start is null, the current time is used. If
     * utc_end is null, utc_start is used.
     *
     * @param type      The type.
     * @param application_id    Application package name.
     * @param label     The label.
     * @param utc_start The start time in UTC ISO 8601 format.
     * @param utc_end   The end time in UTC ISO 8601 format.
     * @param lon       The moves longitude value.
     * @param lat       The moves latitude value.
     * @param activity  The moves activity.
     */
    public Event(String type, String application_id, String label, String utc_start, String utc_end, String lon,
                 String lat, String activity) {
        this.type = type;
        this.application_id = application_id;
        this.label = label;
        this.lon = lon;
        this.lat = lat;
        this.activity = activity;

        if (utc_start == null) {
            this.utc_start = TimeUtils.getUtcString();
        } else {
            this.utc_start = utc_start;
        }

        if (utc_end == null) {
            this.utc_end = this.utc_start;
        } else {
            this.utc_end = utc_end;
        }
    }

    /**
     * Overloaded constructor omitting some fields.
     *
     * @param type      The type.
     * @param application_id    Application package name.
     * @param label     The label.
     * @param utc_start The start time in UTC ISO 8601 format.
     * @param utc_end   The end time in UTC ISO 8601 format.
     */
    public Event(String type, String application_id, String label, String utc_start, String utc_end) {
        this(type, application_id, label, utc_start, utc_end, null, null, null);
    }

    /**
     * Overloaded constructor omitting some fields.
     *
     * @param type      The type.
     * @param label     The label.
     * @param utc_start The start time in UTC ISO 8601 format.
     * @param utc_end   The end time in UTC ISO 8601 format.
     */
    public Event(String type, String label, String utc_start, String utc_end) {
        this(type, null, label, utc_start, utc_end, null, null, null);
    }

    /**
     * Overloaded constructor omitting some fields.
     *
     * @param type The type.
     */
    public Event(String type) {
        this(type, null, null, null, null, null, null, null);
    }

    /**
     * Overloaded constructor omitting some fields.
     *
     * @param type      The type.
     * @param utc_start The start time in UTC ISO 8601 format.
     * @param utc_end   The end time in UTC ISO 8601 format.
     */
    public Event(String type, String utc_start, String utc_end) {
        this(type, null, null, utc_start, utc_end, null, null, null);
    }

    /**
     * Creates and returns a new Event from a compatible json string.
     *
     * @param json The event represented as a json string.
     * @return The event represented as an instance of this class.
     * @throws JSONException
     * @throws IllegalAccessException
     */
    public static Event fromJson(String json) throws JSONException, IllegalAccessException {
        JSONObject object = new JSONObject(json);

        Event event = new Event();

        Iterator<String> iterator = object.keys();

        while (iterator.hasNext()) {
            String key = iterator.next();
            Field field;
            try {
                field = Event.class.getField(key);
            } catch (NoSuchFieldException e) {
                continue;
            }
            int mod = field.getModifiers();
            if (!Modifier.isStatic(mod) && !field.isSynthetic()) {
                field.set(event, object.get(key));
            }
        }

        return event;
    }

    /**
     * Get a list of events from a json array represented by a string.
     *
     * @param json The json array to be converted.
     * @return The list of events.
     * @throws JSONException
     * @throws IllegalAccessException
     */
    public static List<Event> fromJsonArray(String json)
            throws JSONException, IllegalAccessException {
        JSONArray jsonEvents = new JSONArray(json);

        int length = jsonEvents.length();

        ArrayList<Event> events = new ArrayList<>(length);

        for (int i = 0; i < length; i++) {
            events.add(fromJson(jsonEvents.getJSONObject(i).toString()));
        }

        return events;
    }

    /**
     * Convert a list of events to a json array.
     *
     * @param events The list of events to be converted.
     * @return The json array as a String.
     * @throws JSONException
     * @throws IllegalAccessException
     */
    public static String toJsonArray(List<Event> events)
            throws JSONException, IllegalAccessException {
        JSONArray array = new JSONArray();

        for (Event event : events) {
            try{
                array.put(new JSONObject(event.toJson()));
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        Log.i("toJsAr", array.toString());
        return array.toString();
    }

    /**
     * Creates a json representation of this Event.
     *
     * @return The json formatted as a string.
     * @throws IllegalAccessException
     * @throws JSONException
     */
    public String toJson() throws IllegalAccessException, JSONException {
        JSONObject object = new JSONObject();

        for (Field field : getClass().getFields()) {
            int mod = field.getModifiers();

            if (!Modifier.isStatic(mod) && !field.isSynthetic()) {
                object.put(field.getName(), field.get(this));
            }
        }

        return object.toString();
    }

    @Override
    public int compareTo(@NonNull Event another) {
        long thisTime = 0;
        long otherTime = 0;

        try {
            thisTime = TimeUtils.getLong(utc_start, TimeUtils.mysqlFormat);
            otherTime = TimeUtils.getLong(another.utc_start, TimeUtils.mysqlFormat);
        } catch (ParseException e) {
            Log.e(CustomApplication.instance.getPackageName(), Log.getStackTraceString(e));
        }

        if (thisTime < otherTime) {
            return -1;
        } else if (thisTime == otherTime) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * @return A string representing this Event.
     */
    @Override
    public String toString() {
        String result = "";

        for (Field field : getClass().getFields()) {
            int mod = field.getModifiers();

            if (!Modifier.isStatic(mod) && !field.isSynthetic()) {
                try {
                    result += field.getName() + ": " + field.get(this) + "\n";
                } catch (IllegalAccessException e) {
                    Log.e(CustomApplication.instance.getPackageName(), Log.getStackTraceString(e));
                }
            }
        }

        return result;
    }
}
