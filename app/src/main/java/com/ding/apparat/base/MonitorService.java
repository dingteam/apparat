package com.ding.apparat.base;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.ding.apparat.authentication.User;
import com.ding.apparat.monitoring.MonitorHandler;
import com.ding.apparat.sync.Synchronizer;

/**
 * Service that keeps the application alive in the background.
 */
public class MonitorService extends Service {

    /**
     * The globally accessible instance of this service.
     */
    public static MonitorService instance;

    /**
     * The monitor, handling broadcast receivers, content observers and application monitoring.
     */
    private MonitorHandler monitorHandler;

    /**
     * The synchronizer, handling synchronization with, and upload to, the server.
     */
    private Synchronizer synchronizer;

    /**
     * Convenience method for starting this service using the application context.
     */
    public static void start() {
        CustomApplication.instance
                .startService(new Intent(CustomApplication.instance, MonitorService.class));
    }

    /**
     * Convenience method for stopping this service using the application context.
     */
    public static void stop() {
        CustomApplication.instance
                .stopService(new Intent(CustomApplication.instance, MonitorService.class));
    }

    /**
     * Start the monitor handler and the synchronizer.
     */
    private void startRunners() {
        monitorHandler.start();
        synchronizer.start();
    }

    /**
     * Stop the monitor handler and the synchronizer.
     */
    public void stopRunners() {
        monitorHandler.stop();
        synchronizer.stop();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        monitorHandler = new MonitorHandler();
        synchronizer = new Synchronizer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (User.loggedIn()) {
            startRunners();
        } else {
            stopSelf();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        stopRunners();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
