package com.ding.apparat.gui;


import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.SpinnerItem;

public class SpinnerButtonActivity extends ButtonActivity {

    /**
     * Get a spinner entry by id.
     *
     * @param id The spinner id.
     * @return The spinner instance.
     */
    protected Spinner getSpinner(int id) {
        return (Spinner) findViewById(id);
    }


    /**
     * @param id The ID of the spinner.
     * @return null if no item has been selected, the value text otherwise (language independent).
     */
    protected String getSpinnerValue(int id) {
        return getSpinnerValue((Spinner) findViewById(id));
    }

    /**
     * @param spinner The spinner.
     * @return null if no item has been selected, the value text otherwise (language independent).
     */
    protected String getSpinnerValue(Spinner spinner) {
        if (spinner.getSelectedItemPosition() == 0) {
            return null;
        }
        return ((SpinnerItem) spinner.getSelectedItem()).value;
    }

    /**
     * Helper method for initializing spinners.
     *
     * @param spinnerId     The ID of the spinner to initialize.
     * @param viewResource  The resource to initialize the spinner text with.
     * @param valueResource The resource to initialize the spinner values with.
     */
    protected void initializeSpinner(int spinnerId, int viewResource, int valueResource) {
        Spinner spinner = getSpinner(spinnerId);

        String[] view = CustomApplication.instance.getResources().getStringArray(viewResource);
        String[] value = CustomApplication.instance.getResources().getStringArray(valueResource);

        ArrayAdapter<SpinnerItem> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                        SpinnerItem.createFromResource(view, value));

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }
}
