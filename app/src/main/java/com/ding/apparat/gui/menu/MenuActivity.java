package com.ding.apparat.gui.menu;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ding.apparat.R;
import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.authentication.User;
import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.MonitorService;
import com.ding.apparat.base.StartupActivity;
import com.ding.apparat.gui.ButtonActivity;
import com.ding.apparat.gui.ButtonCallable;
import com.ding.apparat.gui.UsageSettingActivity;
import com.ding.apparat.gui.login.LoginActivity;
import com.ding.apparat.monitoring.application.UsageStats;
import com.ding.apparat.moves.MovesActivity;
import com.ding.apparat.moves.MovesAuthenticator;
import com.ding.apparat.moves.MovesHandler;
import com.ding.apparat.moves.MovesTransfer;
import com.ding.apparat.sync.EventTransfer;
import com.ding.apparat.sync.Synchronizer;
import com.ding.apparat.util.TimeUtils;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * The user visible menu for the application.
 */
public class MenuActivity extends ButtonActivity {
    private static boolean ignorePhone = false, ignoreSMS = false;
    private static final String TAG = MenuActivity.class.getSimpleName();


    @Override
    protected void onResume() {
        super.onResume();

        // If the user is on Lollipop or higher and usage access is disabled, redirect to settings.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !UsageStats.enabled()) {
            startActivity(new Intent(this, UsageSettingActivity.class));
            finish();
            return;
        }

        if (!User.loggedIn()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }

        startMonitorService();
        setContentView(R.layout.activity_menu);

        updateSyncTime();

        checkMoves();
    }

    /**
     * Check all dangerous permissions and initiate the MonitorService once the user has been
     * queried regarding them all.
     */
    private void startMonitorService(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && !ignorePhone) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_PHONE_STATE);
        }
        else if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && !ignoreSMS) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, MY_PERMISSIONS_REQUEST_READ_SMS);
        }
        else{
            MonitorService.start();
        }
    }

    /**
     * Handles the callback from asking the user about dangerous permissions.
     * @param requestCode provided by the request call.
     * @param permissions the permission in question.
     * @param grantResults whether or not the user granted access to permission in question.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_PHONE_STATE: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
                    ignorePhone = true;
                }
                break;
            }
            case MY_PERMISSIONS_REQUEST_READ_SMS: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED){
                    ignoreSMS = true;
                }
                break;
            }
            default: {
                break;
            }
        }
    }


    /**
     * Check whether Moves is installed. If it is installed, check if API is accessible. Take the
     * appropriate actions.
     */
    private void checkMoves() {
        // Show one of or none of the Moves buttons.
        if (!MovesHandler.isMovesInstalled()) {
            findViewById(R.id.buttonMovesInstall).setVisibility(View.VISIBLE);
            return;
        }

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    return MovesAuthenticator.validateToken();
                } catch (Exception e) {
                    Log.e(CustomApplication.instance.getPackageName(), Log.getStackTraceString(e));
                    // Could not validate token, but it could possibly still be valid.
                    return true;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if (!aBoolean) {
                    findViewById(R.id.buttonMovesEnable).setVisibility(View.VISIBLE);
                }
            }
        }.execute();
    }

    /**
     * Sets the time of last sync.
     */
    private void updateSyncTime() {
        long l = Synchronizer.getLastSync();
        String s;

        if (l == 0) {
            s = getString(R.string.never_updated);
        } else {
            s = TimeUtils.getDisplayString(l);
        }

        ((TextView) findViewById(R.id.dataSent)).setText(getString(R.string.data_sent) + " " + s);
    }

    /**
     * Executed when the user presses the logout button.
     *
     * @param view The view for the button.
     */
    public void onLogout(final View view) {
        ButtonCallable callable = new ButtonCallable() {
            @Override
            public String callAction() {
                try {
                    Authenticator.logout();
                } catch (InterruptedException e) {
                    return getString(R.string.unknown_error);
                }
                startActivity(new Intent(MenuActivity.this, StartupActivity.class));
                finish();
                return null;
            }
        };

        doTaskWithButtonChange(callable, getString(R.string.logging_out), view);
    }

    /**
     * Executed when the user presses the profile button.
     *
     * @param view The view for the button.
     */
    public void onProfile(View view) {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    /**
     * Executed when the user presses the change password button.
     *
     * @param view The view for the button.
     */
    public void onChangePassword(View view) {
        startActivity(new Intent(this, PasswordChangeActivity.class));
    }

    /**
     * Executed when the user presses any of the moves button.
     *
     * @param view The view for the button.
     */
    public void onMoves(View view) {
        startActivity(new Intent(this, MovesActivity.class));
        finish();
    }

    /**
     * Executed when the user presses the log button.
     *
     * @param view The view for the button.
     */
    public void onLog(View view) {
        startActivity(new Intent(this, EventLogActivity.class));
    }

    /**
     * Executed when the user presses the about application button.
     *
     * @param view The view for the button.
     */
    public void onAbout(View view) {
        startActivity(new Intent(this, AboutActivity.class));
    }

    public void testMoves(View view) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (MovesHandler.uploadRawMoves()) {
                        Log.i(TAG, "Upload of MovesData Successful");
                    } else {
                        Log.i(TAG, "Upload of MovesData Failed");
                    }
                } catch (Exception e) {
                    Log.e(CustomApplication.instance.getPackageName(),
                            Log.getStackTraceString(e));
                }

            }
        }).start();

    }

    public void logToken(View view){
        String token = FirebaseInstanceId.getInstance().getToken();
        String msg = "Firebase token: " + token;
        Log.i(TAG, msg);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
