package com.ding.apparat.gui;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.ding.apparat.R;
import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.SpinnerItem;
import com.ding.apparat.sync.UserTransfer;

import org.json.JSONException;

import java.io.IOException;
import java.util.Map;

/**
 * Base class for activities that uses this form.
 */
public abstract class FormActivity extends SpinnerButtonActivity implements FormActivityInterface {

    /**
     * Validates that all form fields have correct input.
     *
     * @return If the form is valid.
     */
    public boolean validateForm() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.form);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View view = layout.getChildAt(i);
            if (view instanceof Spinner) {
                Spinner spinner = (Spinner) view;
                if (getSpinnerValue(spinner) == null) {
                    return false;
                }
            } else if (view instanceof EditText) {
                EditText text = (EditText) view;
                if (text.getText().length() < 1) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Upload the form data to the server.
     *
     * @return The http status code of the upload.
     * @throws IOException
     * @throws JSONException
     */
    public int uploadForm() throws IOException, JSONException {
        return UserTransfer.uploadForm(getSpinnerValue(R.id.gender), getFieldText(R.id.birth_year),
                getSpinnerValue(R.id.birth_country), getFieldText(R.id.zip_code),
                getSpinnerValue(R.id.occupation), getSpinnerValue(R.id.marital_status),
                getSpinnerValue(R.id.profession));
    }

    /**
     * Initialize the spinner entries.
     */
    public void initializeForm() {
        initializeSpinner(R.id.gender, R.array.genders, R.array.genders_value);
        initializeSpinner(R.id.occupation, R.array.occupations, R.array.occupations_value);
        initializeSpinner(R.id.marital_status, R.array.marital_statuses,
                R.array.marital_statuses_value);
        initializeSpinner(R.id.birth_country, R.array.birth_countries,
                R.array.birth_countries_value);
        initializeSpinner(R.id.profession, R.array.professions, R.array.professions_value);
    }

    /**
     * Download the current values and set them in the form.
     */
    protected void downloadAndSetForm(final int layout) {
        new AsyncTask<Void, Void, Map<String, String>>() {
            @Override
            protected Map<String, String> doInBackground(Void... params) {
                try {
                    return UserTransfer.downloadForm();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Map<String, String> map) {
                super.onPostExecute(map);

                setContentView(layout);

                initializeForm();

                if (map == null) {
                    return;
                }

                for (String s : map.keySet()) {
                    int resId = getResources().getIdentifier(s, "id", getPackageName());
                    View view = findViewById(resId);
                    if (view instanceof Spinner) {
                        Spinner spinner = (Spinner) view;
                        spinner.setSelection(getIndex(spinner, map.get(s)));
                    } else if (view instanceof EditText) {
                        EditText text = (EditText) view;
                        if (!map.get(s).equals("0")) {
                            text.setText(map.get(s));
                        }
                    }
                }
            }

            /**
             * Get the spinner index for the specific text value.
             *
             * @param spinner The spinner to search.
             * @param s The string to search for
             * @return The index of the String in the Spinner, or 0 if missing.
             */
            private int getIndex(Spinner spinner, String s) {
                int count = spinner.getCount();
                for (int i = 0; i < count; i++) {
                    if (((SpinnerItem) spinner.getItemAtPosition(i)).value.equals(s)) {
                        return i;
                    }
                }
                return 0;
            }
        }.execute();
    }


}
