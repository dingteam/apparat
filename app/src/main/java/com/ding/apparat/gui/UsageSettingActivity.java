package com.ding.apparat.gui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ding.apparat.R;
import com.ding.apparat.gui.menu.MenuActivity;
import com.ding.apparat.monitoring.application.UsageStats;

/**
 * The activity for requiring the user to give usage access to this application.
 */
public class UsageSettingActivity extends AppCompatActivity {

    /**
     * Required, unused.
     */
    private static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usage_setting);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (UsageStats.enabled()) {
            startActivity(new Intent(this, MenuActivity.class));
            finish();
        }
    }

    /**
     * Executed when the user presses the usage settings button.
     *
     * @param view The view for the button.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void onUsageSettings(View view) {
        startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS), REQUEST_CODE);
    }
}
