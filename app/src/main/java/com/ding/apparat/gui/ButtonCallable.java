package com.ding.apparat.gui;

/**
 * Interface used together with ButtonActivity. Specifies the method of the object which is to be
 * called by methods in ButtonActivity.
 */
public interface ButtonCallable {

    /**
     * The action to be performed when the user pushes the button.
     *
     * @return The String to be displayed as a toast after completion of the task, or null if no
     * toast should be shown.
     */
    String callAction();
}
