package com.ding.apparat.gui.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ding.apparat.R;

/**
 * The activity for displaying the Terms of Service.
 */
public class TermsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
    }
}
