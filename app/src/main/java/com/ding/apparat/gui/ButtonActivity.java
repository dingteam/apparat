package com.ding.apparat.gui;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.ding.apparat.R;
import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.util.ValidatorUtils;

/**
 * Base class for activities with button actions that display toast error messages.
 */
public abstract class ButtonActivity extends AppCompatActivity {
    protected final static int MY_PERMISSIONS_REQUEST_PHONE_STATE = 110;
    protected final static int MY_PERMISSIONS_REQUEST_READ_SMS = 120;



    /**
     * Get the field text of view.
     *
     * @param id The id of the view.
     * @return The text of the view.
     */
    protected String getFieldText(int id) {
        return ((EditText) findViewById(id)).getText().toString();
    }

    /**
     * Get whether or not a checkbox has been checked.
     *
     * @param id The id of the view.
     * @return Whether it is checked or not.
     */
    protected boolean getCheckboxStatus(int id) {
        return ((CheckBox) findViewById(id)).isChecked();
    }

    /**
     * Do a synchronous action upon a button click.
     *
     * @param callable         The object containing the action to perform.
     * @param buttonTextChange What the button text should change to while performing the action.
     * @param view             The button that is associated with the action.
     */
    protected void doTaskWithButtonChange(ButtonCallable callable, String buttonTextChange,
                                          View view) {
        String buttonTextRegular = ((Button) view).getText().toString();

        if (buttonTextChange != null) {
            ((Button) view).setText(buttonTextChange);
        }

        // Disable the button when operation is running.
        view.setClickable(false);

        String result = callable.callAction();

        if (result != null) {
            Toast.makeText(ButtonActivity.this, result, Toast.LENGTH_SHORT).show();
        }

        ((Button) view).setText(buttonTextRegular);

        view.setClickable(true);
    }

    protected String validateBaseUser(){
        if (!ValidatorUtils.validateEmail(getFieldText(R.id.email))) {
            return getString(R.string.email_wrong_format);
        }

        if (!ValidatorUtils.validate(getFieldText(R.id.password))) {
            return getString(R.string.password_empty);
        }

        if (!ValidatorUtils.validateMatch(getFieldText(R.id.password),
                getFieldText(R.id.confirm_password))) {
            return getString(R.string.password_match);
        }

        return null;
    }

    /**
     * Do an asynchronous action upon a button click.
     *
     * @param callable         The object containing the action to perform.
     * @param buttonTextChange What the button text should change to while performing the action.
     * @param view             The button that is associated with the action.
     */
    protected void doAsyncTaskWithButtonChange(final ButtonCallable callable,
                                               final String buttonTextChange, final View view) {
        final String buttonTextRegular = ((Button) view).getText().toString();

        if (buttonTextChange != null) {
            ((Button) view).setText(buttonTextChange);
        }

        // Disable the button when operation is running.
        view.setClickable(false);
        Authenticator.registerOperation();

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                return callable.callAction();
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);

                if (string != null) {
                    Toast.makeText(ButtonActivity.this, string, Toast.LENGTH_SHORT).show();
                }

                ((Button) view).setText(buttonTextRegular);

                view.setClickable(true);
                Authenticator.unRegisterOperation();
            }
        }.execute();
    }
}
