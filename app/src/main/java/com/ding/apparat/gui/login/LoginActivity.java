package com.ding.apparat.gui.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.ding.apparat.R;
import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.gui.ButtonActivity;
import com.ding.apparat.gui.ButtonCallable;
import com.ding.apparat.gui.menu.MenuActivity;
import com.ding.apparat.storage.PersistentGlobalValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.ValidatorUtils;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * The activity for registering the user.
 */
public class LoginActivity extends ButtonActivity {

    /**
     * If the application has yet been run.
     */
    private static final PersistentValue hasBeenRun = new PersistentGlobalValue("hasBeenRun");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (!hasBeenRun.getBoolean()) {
            showPopup();
            hasBeenRun.setBoolean(true);
        }
    }

    /**
     * Show the initial popup.
     */
    private void showPopup() {
        new AlertDialog.Builder(this).setTitle(R.string.initial_popup_title)
                .setMessage(R.string.initial_popup)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

    /**
     * Executed when the user presses the register button.
     *
     * @param view The view for the button.
     */
    public void onRegister(final View view) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        finish();
    }

    /**
     * Executed when the user presses the login button.
     *
     * @param view The view for the button.
     */
    public void onLogin(final View view) {
        ButtonCallable callable = new ButtonCallable() {
            @Override
            public String callAction() {
                if (!ValidatorUtils.validateEmail(
                        ((EditText) findViewById(R.id.email)).getText().toString())) {
                    return getString(R.string.email_wrong_format);
                }

                if (!ValidatorUtils
                        .validate(((EditText) findViewById(R.id.password)).getText().toString())) {
                    return getString(R.string.password_empty);
                }

                try {
                    switch (Authenticator
                            .login(getFieldText(R.id.email), getFieldText(R.id.password))) {
                        case HttpURLConnection.HTTP_OK:
                            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
                            finish();
                            return null;
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            return getString(R.string.login_wrong_format);
                        case HttpURLConnection.HTTP_FORBIDDEN:
                            return getString(R.string.wrong_credentials);
                        default:
                            return getString(R.string.unknown_error);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return getString(R.string.unknown_error);
                }
            }
        };

        doAsyncTaskWithButtonChange(callable, getString(R.string.logging_in), view);
    }

    /**
     * Executed when the user presses the reset password button.
     *
     * @param view The view for the button.
     */
    public void onResetPassword(View view) {
        startActivity(new Intent(this, PasswordResetActivity.class));
    }
}

