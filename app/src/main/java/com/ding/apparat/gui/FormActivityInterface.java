package com.ding.apparat.gui;

import org.json.JSONException;

import java.io.IOException;

public interface FormActivityInterface {
    boolean validateForm();
    void initializeForm();
    int uploadForm() throws IOException, JSONException;
}
