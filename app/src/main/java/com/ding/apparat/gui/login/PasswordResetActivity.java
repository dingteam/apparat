package com.ding.apparat.gui.login;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ding.apparat.R;
import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.gui.ButtonActivity;
import com.ding.apparat.gui.ButtonCallable;
import com.ding.apparat.util.ValidatorUtils;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * The activity for resetting the user password.
 */
public class PasswordResetActivity extends ButtonActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);
    }

    /**
     * Executed when the user presses the submit button.
     *
     * @param view The view for the button.
     */
    public void onSubmit(View view) {
        ButtonCallable callable = new ButtonCallable() {
            @Override
            public String callAction() {

                if (!ValidatorUtils.validateEmail(getFieldText(R.id.email))) {
                    return getString(R.string.email_wrong_format);
                }

                try {
                    int response = Authenticator.resetPassword(getFieldText(R.id.email));
                    Log.i("FLOWHUNT", "Reponse code: " + response);
                    switch (response) {
                        case HttpURLConnection.HTTP_OK:
                            return getString(R.string.password_reset);
                        case HttpURLConnection.HTTP_FORBIDDEN:
                            return getString(R.string.wrong_email);
                        case HttpURLConnection.HTTP_NOT_FOUND:
                            return getString(R.string.password_reset_wait);
                        default:
                            return getString(R.string.unknown_error);
                    }
                } catch (IOException e) {
                    return getString(R.string.unknown_error);
                }
            }
        };

        doAsyncTaskWithButtonChange(callable, getString(R.string.resetting_password), view);
    }
}
