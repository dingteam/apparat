package com.ding.apparat.gui.menu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ding.apparat.R;

/**
 * The activity for displaying information about the app.
 */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
