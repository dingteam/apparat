package com.ding.apparat.gui.menu;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.sync.EventTransfer;

import java.util.List;

/**
 * The activity for listing events from the database.
 */
public class EventLogActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new AsyncTask<Void, Void, List<Event>>() {
            @Override
            protected List<Event> doInBackground(Void... params) {
                List<Event> events = null;
                try {
                    events = EventTransfer.downloadEvents();
                } catch (Exception e) {
                    Log.e(CustomApplication.instance.getPackageName(), Log.getStackTraceString(e));
                }

                return events;
            }

            @Override
            protected void onPostExecute(List<Event> events) {
                if (events == null) {
                    return;
                }

                ArrayAdapter<Event> itemsAdapter = new ArrayAdapter<>(EventLogActivity.this,
                        android.R.layout.simple_list_item_1, events);

                ListView listview = getListView();
                if (listview != null) {
                    listview.setAdapter(itemsAdapter);
                }
            }
        }.execute();
    }
}
