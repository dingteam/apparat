package com.ding.apparat.gui.menu;

import android.os.Bundle;
import android.view.View;

import com.ding.apparat.R;
import com.ding.apparat.gui.ButtonCallable;
import com.ding.apparat.gui.FormActivity;

import java.net.HttpURLConnection;

/**
 * The form used for gathering user data.
 */
public class ProfileActivity extends FormActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        downloadAndSetForm(R.layout.activity_profile);
    }

    /**
     * Executed when the user presses the submit button.
     *
     * @param view The view for the button.
     */
    public void onSubmit(final View view) {
        ButtonCallable callable = new ButtonCallable() {
            @Override
            public String callAction() {

                if (!validateForm()) {
                    return getString(R.string.form_wrong_format);
                }

                try {
                    switch (uploadForm()) {
                        case HttpURLConnection.HTTP_OK:
                            finish();
                            return null;
                        case HttpURLConnection.HTTP_FORBIDDEN:
                            return getString(R.string.corrupt_login);
                    }
                } catch (Exception ignored) {
                }

                return getString(R.string.unknown_error);
            }
        };

        doAsyncTaskWithButtonChange(callable, getString(R.string.submitting), view);
    }
}
