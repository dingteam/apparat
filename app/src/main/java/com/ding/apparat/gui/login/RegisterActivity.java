package com.ding.apparat.gui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ding.apparat.R;
import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.gui.ButtonCallable;
import com.ding.apparat.gui.FormActivity;
import com.ding.apparat.gui.menu.MenuActivity;
import com.ding.apparat.util.ValidatorUtils;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * The activity for registering a new user.
 */
public class RegisterActivity extends FormActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initializeForm();
        //Reprint the terms_message content in order to change color to parts of it (using fromHtml).
        ((TextView) findViewById(R.id.terms_message)).setText(Html.fromHtml(getString(R.string.terms_message_html)));
    }

    /**
     * Executed when the user presses the register button.
     *
     * @param view The view for the button.
     */
    public void onRegister(final View view) {
        ButtonCallable callable = new ButtonCallable() {
            @Override
            public String callAction() {

                if (!ValidatorUtils.validateEmail(getFieldText(R.id.email))) {
                    return getString(R.string.email_wrong_format);
                }

                if (!ValidatorUtils.validate(getFieldText(R.id.password))) {
                    return getString(R.string.password_empty);
                }

                if (!ValidatorUtils.validateMatch(getFieldText(R.id.password),
                        getFieldText(R.id.confirm_password))) {
                    return getString(R.string.password_match);
                }

                if (!validateForm()) {
                    return getString(R.string.form_wrong_format);
                }
                if(!ValidatorUtils.validateTerms(getCheckboxStatus(R.id.checkbox_terms))){
                    return getString(R.string.terms_unaccepted);
                }

                try {
                    switch (Authenticator
                            .register(getFieldText(R.id.email), getFieldText(R.id.password),
                                    getFieldText(R.id.confirm_password))) {
                        case HttpURLConnection.HTTP_OK:
                            break;
                        case HttpURLConnection.HTTP_CONFLICT:
                            return getString(R.string.email_exists);
                        default:
                            return getString(R.string.unknown_error);
                    }
                } catch (IOException e) {
                    Log.e("REGISTER", e.toString());
                    e.printStackTrace();
                    return getString(R.string.unknown_error);
                }

                // If registration is ok, upload profile as well (suppress errors).
                try {
                    uploadForm();
                } catch (Exception ignored) {
                }

                startActivity(new Intent(RegisterActivity.this, MenuActivity.class));
                finish();
                return null;
            }
        };

        doAsyncTaskWithButtonChange(callable, getString(R.string.registering), view);
    }

    /**
     * Executed when the user presses the terms button.
     *
     * @param view The view for the button.
     */
    public void onTerms(View view) {
        LayoutInflater inflater = getLayoutInflater();
        View termsDialog = inflater.inflate(R.layout.alert_terms, null);

        Button infoOk = (Button) termsDialog.findViewById(R.id.btn_ok);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(termsDialog);
        final AlertDialog alert = builder.show();

        infoOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }
}
