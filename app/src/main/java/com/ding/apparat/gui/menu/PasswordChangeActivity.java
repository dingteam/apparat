package com.ding.apparat.gui.menu;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.ding.apparat.R;
import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.gui.ButtonActivity;
import com.ding.apparat.gui.ButtonCallable;
import com.ding.apparat.util.ValidatorUtils;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * The form for changing the users password.
 */
public class PasswordChangeActivity extends ButtonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
    }

    /**
     * Executed when the submit button is pressed.
     *
     * @param view The view for the button.
     */
    public void onSubmit(View view) {
        ButtonCallable callable = new ButtonCallable() {
            @Override
            public String callAction() {

                String oldPassword = ((EditText) findViewById(R.id.password)).getText().toString();
                String newPassword =
                        ((EditText) findViewById(R.id.new_password)).getText().toString();
                String newPasswordConfirm =
                        ((EditText) findViewById(R.id.confirm_new_password)).getText().toString();

                if (!ValidatorUtils.validate(oldPassword)) {
                    return getString(R.string.old_password_empty);
                }

                if (!ValidatorUtils.validate(newPassword)) {
                    return getString(R.string.new_password_empty);
                }

                if (!ValidatorUtils.validateMatch(newPassword, newPasswordConfirm)) {
                    return getString(R.string.new_password_match);
                }

                try {
                    switch (Authenticator.changePassword(getFieldText(R.id.password),
                            getFieldText(R.id.new_password),
                            getFieldText(R.id.confirm_new_password))) {
                        case HttpURLConnection.HTTP_OK:
                            finish();
                            return getString(R.string.change_password_success);
                        case HttpURLConnection.HTTP_FORBIDDEN:
                            return getString(R.string.old_password_wrong);
                        default:
                            return getString(R.string.unknown_error);
                    }
                } catch (IOException e) {
                    return getString(R.string.unknown_error);
                }
            }
        };

        doAsyncTaskWithButtonChange(callable, getString(R.string.changing_password), view);
    }
}
