package com.ding.apparat.monitoring.screen;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.MonitorReceiver;
import com.ding.apparat.storage.EventHandler;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.TimeUtils;

/**
 * Broadcast receiver for screen on and off events.
 */
public class ScreenReceiver extends MonitorReceiver {

    private final PersistentValue previousTime = new PersistentUserValue("previousTime");

    /**
     * Initialize a new instance of this class.
     */
    public ScreenReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        registerFilter(filter);

        if (previousTime.getLong() == 0) {
            previousTime.setLong(TimeUtils.now());
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case Intent.ACTION_SCREEN_ON:
                previousTime.setLong(TimeUtils.now());
                break;
            case Intent.ACTION_SCREEN_OFF:
                EventHandler.writeEvent(
                        new Event(Event.TYPE_SCREEN, TimeUtils.getUtcString(previousTime.getLong()),
                                TimeUtils.getUtcString()));
                break;
        }
    }
}
