package com.ding.apparat.monitoring.message;

import android.net.Uri;

import com.ding.apparat.base.Event;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;

/**
 * Handles outgoing MMS messages.
 */
public class MmsContentObserver extends MessageContentObserver {

    /**
     * The URI for querying sent mms.
     */
    private static final String QUERY_URI = "content://mms/sent/";

    /**
     * The persistent object for storing the previous mms.
     */
    private static final PersistentValue previousID = new PersistentUserValue("previousID");

    /**
     * Implement super constructor.
     */
    public MmsContentObserver() {
        super();
    }

    @Override
    protected Uri queryUri() {
        return Uri.parse(QUERY_URI);
    }

    @Override
    protected String typeName() {
        return Event.TYPE_MMS;
    }

    @Override
    protected PersistentValue getPreviousID() {
        return previousID;
    }
}
