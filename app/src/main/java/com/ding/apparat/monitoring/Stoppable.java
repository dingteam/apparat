package com.ding.apparat.monitoring;

/**
 * Interface defining a class to be stoppable in some sense.
 */
public interface Stoppable {

    /**
     * Stop what this stoppable is doing.
     */
    void stop();
}
