package com.ding.apparat.monitoring.message;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.MonitorReceiver;
import com.ding.apparat.storage.EventHandler;

/**
 * Receiver for incoming MMS messages.
 */
public class MmsReceiver extends MonitorReceiver {

    /**
     * Create a new instance of this class.
     */
    public MmsReceiver() {
        try {
            IntentFilter filter = new IntentFilter("android.provider.Telephony.WAP_PUSH_RECEIVED",
                    "application/vnd.wap.mms-message");
            registerFilter(filter);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            Log.e(CustomApplication.instance.getPackageName(), Log.getStackTraceString(e));
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        EventHandler.writeEvent(new Event(Event.TYPE_MMS + "," + Event.TYPE_INCOMING));
    }
}
