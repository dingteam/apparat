package com.ding.apparat.monitoring;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.monitoring.application.ApplicationMonitor;
import com.ding.apparat.monitoring.music.MusicMonitor;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is the part of this application that handles all pollers.
 */
public class Poller implements Stoppable {

    /**
     * How often the poller should execute (in milliseconds).
     */
    private static final long SERVICE_PERIOD = 5000;

    /**
     * The list of registered pollers.
     */
    private final LinkedList<Pollable> pollers;

    /**
     * Used for shutting of the poller when the screen goes down (to save battery).
     */
    private final ScreenReceiver receiver;

    /**
     * The timer used for polling.
     */
    private Timer timer;

    /**
     * Initializes a new Poller.
     */
    public Poller() {

        pollers = new LinkedList<>();

        // Only start application monitor if < 5.0
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            pollers.add(new ApplicationMonitor());
        }

        pollers.add(new MusicMonitor());

        receiver = new ScreenReceiver();

        startTimer();
    }

    /**
     * Start the timer.
     */
    private void startTimer() {
        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Authenticator.registerOperation();

                for (Pollable p : pollers) {
                    p.pollAction();
                }

                Authenticator.unRegisterOperation();
            }
        }, 0, SERVICE_PERIOD);
    }

    /**
     * Stop the timer.
     */
    private void stopTimer() {
        timer.cancel();

        for (Pollable p : pollers) {
            p.onStop();
        }
    }

    @Override
    public void stop() {
        stopTimer();
        receiver.stop();
    }

    /**
     * This class handles screen on and off events for this class, calling the correct methods when
     * these events occur.
     */
    private class ScreenReceiver extends MonitorReceiver {

        /**
         * Registers this receiver.
         */
        public ScreenReceiver() {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_SCREEN_ON);
            registerFilter(filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Intent.ACTION_SCREEN_ON:
                    startTimer();
                    break;
                case Intent.ACTION_SCREEN_OFF:
                    stopTimer();
                    break;
            }
        }
    }
}
