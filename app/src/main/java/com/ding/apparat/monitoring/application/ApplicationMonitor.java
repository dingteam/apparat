package com.ding.apparat.monitoring.application;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.Pollable;
import com.ding.apparat.storage.EventHandler;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.ApplicationUtils;
import com.ding.apparat.util.TimeUtils;

/**
 * Monitors running foreground applications by polling.
 */
public class ApplicationMonitor implements Pollable {

    /**
     * Supplies core functionality for this class.
     */
    private final ActivityManager activityManager;

    /**
     * Used for determining if the screen is interactive or not.
     */
    private final PowerManager powerManager;

    /**
     * The previously running foreground application.
     */
    private final PersistentValue previous = new PersistentUserValue("previous");

    /**
     * The previous time a new application was detected.
     */
    private final PersistentValue previousTime = new PersistentUserValue("previousTime");

    /**
     * Create the monitor.
     */
    public ApplicationMonitor() {
        activityManager = (ActivityManager) CustomApplication.instance
                .getSystemService(Context.ACTIVITY_SERVICE);

        powerManager =
                (PowerManager) CustomApplication.instance.getSystemService(Context.POWER_SERVICE);
    }

    /**
     * @return The foreground application
     */
    private String getForegroundApplication() {
        boolean screenOn;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            screenOn = powerManager.isInteractive();
        } else {
            screenOn = powerManager.isScreenOn();
        }

        if (!screenOn) {
            return null;
        }

        return activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
    }

    @Override
    public synchronized void pollAction() {
        processDiffs(getForegroundApplication());
    }

    /**
     * Helper method for recordTaskChanges() and recordProcessChanges(). Processes the differences
     * as to determine started and closed applications.
     *
     * @param current The newly retrieved foreground package name.
     */
    private synchronized void processDiffs(String current) {
        String previous = this.previous.getString();

        if (current == null) {
            if (previous != null) {
                writeEvent(previous, previousTime.getLong(), TimeUtils.now());
            }
        } else if (previous == null) {
            previousTime.setLong(TimeUtils.now());
        } else if (!current.equals(previous)) {
            writeEvent(previous, previousTime.getLong(), TimeUtils.now());
            previousTime.setLong(TimeUtils.now());
        } else {
            return;
        }

        this.previous.setString(current);
    }

    /**
     * Helper function for writing application event to file.
     *
     * @param packageName The package name.
     * @param start       The start time.
     * @param end         The end time.
     */
    private void writeEvent(String packageName, long start, long end) {
        EventHandler.writeEvent(
                new Event(Event.TYPE_APPLICATION, packageName, ApplicationUtils.getApplicationName(packageName),
                        TimeUtils.getUtcString(start), TimeUtils.getUtcString(end)));
    }

    @Override
    public void onStop() {
        processDiffs(null);
    }
}
