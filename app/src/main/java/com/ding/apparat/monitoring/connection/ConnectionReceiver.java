package com.ding.apparat.monitoring.connection;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.MonitorReceiver;
import com.ding.apparat.storage.EventHandler;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.TimeUtils;

/**
 * Broadcast receiver for connection events.
 */
public class ConnectionReceiver extends MonitorReceiver {

    /**
     * Extended type for disconnection (NetworkInfo's getType() does not have a value for this).
     */
    private static final int TYPE_DISCONNECTED = -1;

    /**
     * The previous connection.
     */
    private final PersistentValue previous = new PersistentUserValue("previous");

    /**
     * The previous time a new connection was detected.
     */
    private final PersistentValue previousTime = new PersistentUserValue("previousTime");

    /**
     * Supplies core functionality for this receiver.
     */
    private final ConnectivityManager cm;

    /**
     * Initialize a new connection receiver.
     */
    public ConnectionReceiver() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerFilter(filter);

        cm = (ConnectivityManager) CustomApplication.instance
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    /**
     * @return The name of the current network connection
     */
    private String getNetworkTypeName() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        // Get type of connection
        int type;
        if (activeNetwork == null || !activeNetwork.isConnected()) {
            type = TYPE_DISCONNECTED;
        } else {
            type = activeNetwork.getType();
        }

        switch (type) {
            case TYPE_DISCONNECTED:
                return null;
            case ConnectivityManager.TYPE_MOBILE:
                return activeNetwork.getSubtypeName();
            default:
                return activeNetwork.getTypeName();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String current = getNetworkTypeName();
        String previous = this.previous.getString();

        if (current == null) {
            if (previous != null) {
                writeEvent(previous, previousTime.getLong(), TimeUtils.now());
            }
        } else if (previous == null) {
            previousTime.setLong(TimeUtils.now());
        } else if (!current.equals(previous)) {
            writeEvent(previous, previousTime.getLong(), TimeUtils.now());
            previousTime.setLong(TimeUtils.now());
        } else {
            return;
        }

        this.previous.setString(current);
    }

    /**
     * Helper method for writing connection events
     *
     * @param connectionName The connection name.
     * @param start          The start time.
     * @param end            The end time.
     */
    private void writeEvent(String connectionName, long start, long end) {
        EventHandler.writeEvent(
                new Event(Event.TYPE_CONNECTION, connectionName, TimeUtils.getUtcString(start),
                        TimeUtils.getUtcString(end)));
    }
}
