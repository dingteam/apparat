package com.ding.apparat.monitoring;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.IdempotentRunner;
import com.ding.apparat.monitoring.connection.ConnectionReceiver;
import com.ding.apparat.monitoring.message.MmsContentObserver;
import com.ding.apparat.monitoring.message.MmsReceiver;
import com.ding.apparat.monitoring.message.SmsContentObserver;
import com.ding.apparat.monitoring.message.SmsReceiver;
import com.ding.apparat.monitoring.phone.PhoneReceiver;
import com.ding.apparat.monitoring.screen.ScreenReceiver;

import java.util.LinkedList;

/**
 * Handles all monitoring.
 */
public class MonitorHandler extends IdempotentRunner {

    /**
     * List of all monitoring components.
     */
    private LinkedList<Stoppable> monitors;

    @Override
    protected void startAction() {
        monitors = new LinkedList<>();

        //Start all receivers we have been granted access to
        if(ContextCompat.checkSelfPermission(CustomApplication.instance, Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED) {
            monitors.add(new ConnectionReceiver());
        }
        if(ContextCompat.checkSelfPermission(CustomApplication.instance, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            monitors.add(new PhoneReceiver());
        }
        if(ContextCompat.checkSelfPermission(CustomApplication.instance, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
            monitors.add(new SmsReceiver());
        }
        if(ContextCompat.checkSelfPermission(CustomApplication.instance, Manifest.permission.RECEIVE_MMS) == PackageManager.PERMISSION_GRANTED) {
            monitors.add(new MmsReceiver());
        }
        if(ContextCompat.checkSelfPermission(CustomApplication.instance, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
            monitors.add(new SmsContentObserver());
            monitors.add(new MmsContentObserver());
        }
        monitors.add(new ScreenReceiver());
        monitors.add(new Poller());
    }

    @Override
    protected void stopAction() {
        for (Stoppable s : monitors) {
            s.stop();
        }
    }
}
