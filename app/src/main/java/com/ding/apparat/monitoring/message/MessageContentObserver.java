package com.ding.apparat.monitoring.message;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.BaseColumns;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.Stoppable;
import com.ding.apparat.storage.EventHandler;
import com.ding.apparat.storage.PersistentValue;

/**
 * Handles outgoing SMS or MMS messages.
 */
abstract class MessageContentObserver extends ContentObserver implements Stoppable {

    /**
     * URI for SMS content providers.
     */
    private static final String CONTENT_PROVIDER_URI_SMS = "content://sms/";

    /**
     * URI for MMS-SMS content providers.
     */
    private static final String CONTENT_PROVIDER_URI_MMS_SMS = "content://mms-sms/";

    /**
     * URI for MMS content providers.
     */
    private static final String CONTENT_PROVIDER_URI_MMS = "content://mms/";

    /**
     * The column name for date.
     */
    private static final String DATE = "date";

    /**
     * The sort order
     */
    private static final String SORT_ORDER = DATE + " DESC";

    /**
     * Creates a new instance of this class.
     */
    MessageContentObserver() {
        super(new Handler());

        // Set up previous ID
        Cursor cursor = CustomApplication.instance.getContentResolver()
                .query(queryUri(), new String[]{BaseColumns._ID}, null, null, DATE + " DESC");
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            getPreviousID().setLong(cursor.getLong(cursor.getColumnIndex(BaseColumns._ID)));
        }
        cursor.close();

        // Set what content providers to listen on.
        CustomApplication.instance.getContentResolver()
                .registerContentObserver(Uri.parse(CONTENT_PROVIDER_URI_SMS), true, this);
        CustomApplication.instance.getContentResolver()
                .registerContentObserver(Uri.parse(CONTENT_PROVIDER_URI_MMS_SMS), true, this);
        CustomApplication.instance.getContentResolver()
                .registerContentObserver(Uri.parse(CONTENT_PROVIDER_URI_MMS), true, this);
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);

        Cursor cursor = CustomApplication.instance.getContentResolver()
                .query(queryUri(), new String[]{BaseColumns._ID, DATE}, null, null, SORT_ORDER);

        if (cursor.moveToFirst()) {
            if (getPreviousID().getLong() !=
                    cursor.getLong(cursor.getColumnIndex(BaseColumns._ID))) {
                EventHandler.writeEvent(new Event(typeName() + "," + Event.TYPE_OUTGOING));
            }

            getPreviousID().setLong(cursor.getLong(cursor.getColumnIndex(BaseColumns._ID)));
        }

        cursor.close();
    }

    /**
     * @return The URI to query.
     */
    protected abstract Uri queryUri();

    /**
     * @return The current JSON type name ("sms" or "mms").
     */
    protected abstract String typeName();

    /**
     * @return The id of the last message (used for avoiding duplicates).
     */
    protected abstract PersistentValue getPreviousID();

    @Override
    public void stop() {
        CustomApplication.instance.getContentResolver().unregisterContentObserver(this);
    }
}
