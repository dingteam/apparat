package com.ding.apparat.monitoring.application;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.ApplicationUtils;
import com.ding.apparat.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * UsageStatsManager functionality (ApplicationMonitor equivalent) for android >= 5.0.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
public class UsageStats {

    /**
     * The persistent object for the last usage stats event timestamp.
     */
    private static final PersistentValue lastTimestamp = new PersistentUserValue("lastTimestamp");

    /**
     * The persistent object for the last usage stats query event timestamp.
     */
    private static final PersistentValue lastQueryTimestamp =
            new PersistentUserValue("lastQueryTimestamp");

    /**
     * Set the last event to the last queried event. Should be used when the retrieved data has been
     * safely written to server.
     */
    public static void setLastTimestamp() {
        lastTimestamp.setLong(lastQueryTimestamp.getLong());
    }

    /**
     * Get all usage events since last timestamp.
     *
     * @return A list of events.
     */
    public static List<Event> getUsageStats() {
        List<Event> list = new ArrayList<>();

        UsageStatsManager manager = (UsageStatsManager) CustomApplication.instance
                .getSystemService(Context.USAGE_STATS_SERVICE);

        UsageEvents events =
                manager.queryEvents(lastTimestamp.getLong(), System.currentTimeMillis());

        UsageEvents.Event previous = null;

        while (events.hasNextEvent()) {
            UsageEvents.Event event = new UsageEvents.Event();
            events.getNextEvent(event);

            switch (event.getEventType()) {
                case UsageEvents.Event.MOVE_TO_FOREGROUND:
                    break;
                case UsageEvents.Event.MOVE_TO_BACKGROUND:
                    if (previous != null &&
                            previous.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND &&
                            previous.getPackageName().equals(event.getPackageName())) {
                        list.add(new Event(Event.TYPE_APPLICATION,
                                event.getPackageName(),
                                ApplicationUtils.getApplicationName(event.getPackageName()),
                                TimeUtils.getUtcString(previous.getTimeStamp()),
                                TimeUtils.getUtcString(event.getTimeStamp())));
                    }
                    break;
                default:
                    continue;
            }

            previous = event;
        }

        if (previous != null) {
            lastQueryTimestamp.setLong(previous.getTimeStamp());
        }

        // Remove unnecessary entries.
        List<Event> result = new ArrayList<>();
        int length = list.size();
        Event prev = null;
        for (int i = 0; i < length; ++i) {
            Event cur = list.get(i);

            if (prev == null) {
                prev = cur;
                continue;
            }

            if (prev.label.equals(cur.label) && prev.utc_end.equals(cur.utc_start)) {
                cur.utc_start = prev.utc_start;
            } else {
                result.add(prev);
            }

            prev = cur;
        }
        // Last element
        if(prev != null){
            result.add(prev);
        }

        return result;
    }

    /**
     * @return If the usage stats settings is disabled.
     */
    public static boolean enabled() {
        try {
            PackageManager packageManager = CustomApplication.instance.getPackageManager();
            ApplicationInfo applicationInfo = packageManager
                    .getApplicationInfo(CustomApplication.instance.getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) CustomApplication.instance
                    .getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsManager
                    .checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid,
                            applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
