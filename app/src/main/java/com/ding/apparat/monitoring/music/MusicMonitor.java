package com.ding.apparat.monitoring.music;

import android.content.Context;
import android.media.AudioManager;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.Pollable;
import com.ding.apparat.storage.EventHandler;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.TimeUtils;

/**
 * Monitors when music starts and stops playing.
 */
public class MusicMonitor implements Pollable {

    /**
     * Supplies core functionality for this class.
     */
    private final AudioManager manager;

    /**
     * The previous state of music playing.
     */
    private final PersistentValue previous = new PersistentUserValue("previous");

    /**
     * The previous time a music change was detected.
     */
    private final PersistentValue previousTime = new PersistentUserValue("previousTime");

    /**
     * Initializes a new instance of this class.
     */
    public MusicMonitor() {
        manager = (AudioManager) CustomApplication.instance.getSystemService(Context.AUDIO_SERVICE);

        if (previousTime.getLong() == 0) {
            previousTime.setLong(TimeUtils.now());
        }
    }

    @Override
    public void pollAction() {
        boolean current = manager.isMusicActive();
        boolean previous = this.previous.getBoolean();
        long previousTime = this.previousTime.getLong();

        if (current != previous) {
            if (current) {
                this.previousTime.setLong(TimeUtils.now());
            } else {
                EventHandler.writeEvent(
                        new Event(Event.TYPE_MUSIC, TimeUtils.getUtcString(previousTime),
                                TimeUtils.getUtcString()));
            }
        }

        this.previous.setBoolean(current);
    }

    @Override
    public void onStop() {
        // Nothing to do here.
    }
}
