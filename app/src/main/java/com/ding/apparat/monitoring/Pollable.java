package com.ding.apparat.monitoring;

/**
 * An interface for application modules that can be used for polling.
 */
public interface Pollable {

    /**
     * The action which is to be executed upon each polling "tick".
     */
    void pollAction();

    /**
     * Should be called when polling stops occurring. Used for ensuring pollable components
     * functions correctly.
     */
    void onStop();
}
