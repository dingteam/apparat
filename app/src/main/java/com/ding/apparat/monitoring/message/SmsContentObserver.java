package com.ding.apparat.monitoring.message;

import android.net.Uri;

import com.ding.apparat.base.Event;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;

/**
 * Handles outgoing SMS messages.
 */
public class SmsContentObserver extends MessageContentObserver {

    /**
     * The URI for querying sent sms.
     */
    private static final String QUERY_URI = "content://sms/sent/";

    /**
     * The persistent object for storing the previous sms.
     */
    private static final PersistentValue previousID = new PersistentUserValue("previousID");

    /**
     * Implement super constructor.
     */
    public SmsContentObserver() {
        super();
    }

    @Override
    protected Uri queryUri() {
        return Uri.parse(QUERY_URI);
    }

    @Override
    protected String typeName() {
        return Event.TYPE_SMS;
    }

    @Override
    protected PersistentValue getPreviousID() {
        return previousID;
    }
}
