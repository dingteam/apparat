package com.ding.apparat.monitoring.phone;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.TelephonyManager;

import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.MonitorReceiver;
import com.ding.apparat.storage.EventHandler;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.TimeUtils;

/**
 * Broadcast receiver for call events.
 */
public class PhoneReceiver extends MonitorReceiver {

    /**
     * Keeps the last state with which this receiver was invoked.
     */
    private final PersistentValue lastState = new PersistentUserValue("lastState");

    /**
     * If the most recent established call is/was incoming.
     */
    private final PersistentValue isIncoming = new PersistentUserValue("isIncoming");

    /**
     * The previous time a new phone call was detected.
     */
    private final PersistentValue previousTime = new PersistentUserValue("previousTime");

    public PhoneReceiver() {
        IntentFilter filter = new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        registerFilter(filter);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String stateStr = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        int state;

        if (stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            state = TelephonyManager.CALL_STATE_IDLE;
        } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            state = TelephonyManager.CALL_STATE_OFFHOOK;
        } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            state = TelephonyManager.CALL_STATE_RINGING;
        } else {
            return;
        }

        // Check if state has changed since last invocation
        if (state == lastState.getInt()) {
            return;
        }

        // Different routes through a phone call
        switch (lastState.getInt()) {
            case TelephonyManager.CALL_STATE_IDLE:
                // Started outgoing call
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    isIncoming.setBoolean(false);
                    previousTime.setLong(TimeUtils.now());
                }
                break;
            case TelephonyManager.CALL_STATE_RINGING:
                // Answered incoming call
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    isIncoming.setBoolean(true);
                    previousTime.setLong(TimeUtils.now());
                }
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                // Hung up
                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    String subtype;
                    if (isIncoming.getBoolean()) {
                        subtype = Event.TYPE_INCOMING;
                    } else {
                        subtype = Event.TYPE_OUTGOING;
                    }
                    writeEvent(subtype, previousTime.getLong(), TimeUtils.now());
                }
        }

        lastState.setInt(state);
    }

    /**
     * Helper function for writing phone events.
     */
    private void writeEvent(String subtype, long start, long end) {
        EventHandler.writeEvent(
                new Event(Event.TYPE_PHONE + "," + subtype, TimeUtils.getUtcString(start),
                        TimeUtils.getUtcString(end)));
    }
}
