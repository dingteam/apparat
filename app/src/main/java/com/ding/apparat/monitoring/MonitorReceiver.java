package com.ding.apparat.monitoring;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;

import com.ding.apparat.base.CustomApplication;

/**
 * Base class for all receivers used for monitoring (sms, mms, phone, network).
 */
public abstract class MonitorReceiver extends BroadcastReceiver implements Stoppable {

    /**
     * Set up this receiver with specified intent filter.
     *
     * @param filter The filter.
     */
    protected void registerFilter(IntentFilter filter) {
        CustomApplication.instance.registerReceiver(this, filter);
    }

    @Override
    public void stop() {
        CustomApplication.instance.unregisterReceiver(this);
    }
}
