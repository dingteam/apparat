package com.ding.apparat.monitoring.message;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.ding.apparat.base.Event;
import com.ding.apparat.monitoring.MonitorReceiver;
import com.ding.apparat.storage.EventHandler;

/**
 * Receiver for incoming SMS messages.
 */
public class SmsReceiver extends MonitorReceiver {

    /**
     * Create a new instance of this class.
     */
    public SmsReceiver() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        registerFilter(filter);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        EventHandler.writeEvent(new Event(Event.TYPE_SMS + "," + Event.TYPE_INCOMING));
    }
}
