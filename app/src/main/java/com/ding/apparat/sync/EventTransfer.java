package com.ding.apparat.sync;

import android.os.Build;
import android.util.Log;

import com.ding.apparat.authentication.User;
import com.ding.apparat.base.Event;
import com.ding.apparat.http.GetRequest;
import com.ding.apparat.http.PostRequest;
import com.ding.apparat.monitoring.application.UsageStats;
import com.ding.apparat.moves.MovesAuthenticator;
import com.ding.apparat.moves.MovesHandler;
import com.ding.apparat.storage.EventHandler;

import org.json.JSONException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for handling uploads and downloads of events to the database (through the application
 * API).
 */
public class EventTransfer {

    /**
     * The URL for uploading to server.
     */
    private static final String UPLOAD_EVENT_URL = GetRequest.API_BASE + "uploadEvents";

    /**
     * The URL for downloading from server.
     */
    private static final String DOWNLOAD_EVENT_URL = GetRequest.API_BASE + "downloadEvents";


    private static final String TAG = EventTransfer.class.getSimpleName();

    /**
     * Synchronize all recorded events with the server.
     *
     * @return If any events where sent to the server.
     */
    public static boolean uploadEvents()
            throws IOException, JSONException, ParseException, IllegalAccessException {
        // Synchronize with EventHandler so that no writing of events
        // can occur during the upload operation.
        Log.i(TAG, "uploadEvents");
        synchronized (EventHandler.class) {
            List<Event> list = new ArrayList<>();

            List<Event> events = EventHandler.readEvents();

            if (events != null) {
                list.addAll(events);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && UsageStats.enabled()) {
                List<Event> usageStats = UsageStats.getUsageStats();
                if (usageStats != null) {
                    list.addAll(usageStats);
                }
            }

            if (list.isEmpty()) {
                Log.i(TAG, "EventList empty");
                return false;
            }

            // Append moves if applicable.
            List<Event> writeBack = null;
            if (MovesAuthenticator.validateToken()) {
                writeBack = MovesHandler.appendMoves(list);
            }

            // Post file data.
            if (postEvents(list)) {
                EventHandler.clearEvents();
                UsageStats.setLastTimestamp();
                if (writeBack != null) {
                    EventHandler.writeEvents(writeBack);
                }
                return true;
            }

            return false;
        }
    }

    /**
     * @return A List of all the events present on the server for the current user.
     */
    public static List<Event> downloadEvents()
            throws IllegalAccessException, JSONException, IOException {
        PostRequest connection = null;

        try {
            Log.d(TAG, "DOWNLOAD_EVENT_URL: " + DOWNLOAD_EVENT_URL);
            connection = new PostRequest(DOWNLOAD_EVENT_URL);
            connection.addParameter("email", User.getUser());
            connection.addParameter("login_uuid", User.getLoginUuid());

            if (connection.getResponseStatus() == HttpURLConnection.HTTP_OK) {
                return Event.fromJsonArray(connection.getResponse());
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return null;
    }

    /**
     * Post the specified data to the server.
     *
     * @param events The JSONArray containing data.
     * @return If the operation succeeded.
     */
    private static boolean postEvents(List<Event> events)
            throws IOException, JSONException, IllegalAccessException {
        Log.i(TAG, "postEvents");
        if (events.isEmpty()) {
            Log.i(TAG, "events List empty");
            return false;
        }

        PostRequest connection = null;

        try {
            Log.d(TAG, "UPLOAD_EVENT_URL: " + UPLOAD_EVENT_URL);
            connection = new PostRequest(UPLOAD_EVENT_URL);
            connection.addParameter("data", Event.toJsonArray(events));
            connection.addParameter("email", User.getUser());
            connection.addParameter("login_uuid", User.getLoginUuid());
            Log.i(TAG, "email: " + User.getUser());
            Log.i(TAG, "login_uuid: " + User.getLoginUuid());

            if (connection.getResponseStatus() != HttpURLConnection.HTTP_OK) {
                Log.e(TAG, "Response status: " + connection.getResponseStatus());
                return false;
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return true;
    }
}
