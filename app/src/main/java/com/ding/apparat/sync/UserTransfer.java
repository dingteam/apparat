package com.ding.apparat.sync;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.ding.apparat.authentication.User;
import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.http.GetRequest;
import com.ding.apparat.http.PostRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Transfer user data to and from the application.
 */
public class UserTransfer {
    private static final String TAG = UserTransfer.class.getSimpleName();

    /**
     * The URL for posting updates.
     */
    private static final String UPLOAD_FORM_URL = GetRequest.API_BASE + "updateInformation";

    /**
     * The URL for downloading information about user.
     */
    private static final String DOWNLOAD_FORM_URL = GetRequest.API_BASE + "downloadInformation";

    /**
     * Upload user information to the database.
     *
     * @param birth_year     The users birth year.
     * @param birth_country  The users birth country.
     * @param zip_code       The users zip code
     * @param occupation     The users occupation
     * @param marital_status The users marital status
     * @param profession     The users profession
     * @return The http response code of the underlying information upload attempt.
     */
    public static int uploadForm(String gender, String birth_year, String birth_country,
                                 String zip_code, String occupation, String marital_status,
                                 String profession) throws IOException, JSONException {
        PostRequest connection = null;

        try {
            connection = new PostRequest(UPLOAD_FORM_URL);

            JSONObject object = new JSONObject();

            object.put("gender", gender);
            object.put("birth_year", birth_year);
            object.put("birth_country", birth_country);
            object.put("zip_code", zip_code);
            object.put("occupation", occupation);
            object.put("marital_status", marital_status);
            object.put("profession", profession);
            object.put("model", Build.MODEL);
            object.put("brand", Build.BRAND);
            object.put("operator", ((TelephonyManager) CustomApplication.instance
                    .getSystemService(Context.TELEPHONY_SERVICE)).getNetworkOperatorName());

            Log.i(TAG, "email: " + User.getUser());
            Log.i(TAG, "login_uuid: " + User.getLoginUuid());
            Log.i(TAG, "data: " + object.toString());

            connection.addParameter("email", User.getUser());
            connection.addParameter("login_uuid", User.getLoginUuid());
            connection.addParameter("data", object.toString());
            return connection.getResponseStatus();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Download form/user data from the server.
     *
     * @return A map containing the user data.
     */
    public static Map<String, String> downloadForm() throws IOException, JSONException {
        Map<String, String> result = new HashMap<>();

        PostRequest connection = null;

        try {
            connection = new PostRequest(DOWNLOAD_FORM_URL);
            connection.addParameter("email", User.getUser());
            connection.addParameter("login_uuid", User.getLoginUuid());

            if (connection.getResponseStatus() == HttpURLConnection.HTTP_OK) {
                JSONObject object = new JSONObject(connection.getResponse());
                Iterator<String> keys = object.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    result.put(key, object.getString(key));
                }
                return result;
            } else {
                return null;
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
