package com.ding.apparat.sync;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.provider.Settings;
import android.util.Log;

import com.ding.apparat.BuildConfig;
import com.ding.apparat.authentication.Authenticator;
import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.IdempotentRunner;
import com.ding.apparat.moves.MovesHandler;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.TimeUtils;

import java.util.Calendar;
import java.util.Random;

/**
 * Class for issuing synchronization with server at regular intervals.
 */
public class Synchronizer extends IdempotentRunner {

    /**
     * With what interval should the synchronization occur. Should be specified as one of the
     * AlarmManager constants.
     */
    private static final long SYNC_INTERVAL = BuildConfig.SYNC_INTERVAL;

    /**
     * The shared preference key for the last synchronization time_utc.
     */
    private static final PersistentValue lastSync = new PersistentUserValue("sync");

    private static final String TAG = Synchronizer.class.getSimpleName();

    private static final int ALARM_ID_EVENT = 0;
    private static final int ALARM_ID_MOVES = 1;

    /**
     * The alarm manager for repeating synchronizations.
     */
    private AlarmManager manager;

    /**
     * The intent which the manager fires.
     */
    private PendingIntent alarmIntent;
    private PendingIntent movesAlarmIntent;

    /**
     * Set the time_utc for last sync.
     */
    private static void setLastSync() {
        lastSync.setLong(TimeUtils.now());
    }

    /**
     * @return The time_utc for last sync.
     */
    public static long getLastSync() {
        return lastSync.getLong();
    }

    @Override
    protected void startAction() {
        manager = (AlarmManager) CustomApplication.instance.getSystemService(Context.ALARM_SERVICE);

        //Alarm managing event synchronization
        Intent eventIntent = new Intent(CustomApplication.instance, AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(CustomApplication.instance, ALARM_ID_EVENT, eventIntent, 0);

        manager.setRepeating(AlarmManager.ELAPSED_REALTIME, 5000L, SYNC_INTERVAL, alarmIntent);

        //Alarm managing Moves synchronization
        Intent movesIntent = new Intent(CustomApplication.instance, MovesAlarmReceiver.class);
        movesAlarmIntent = PendingIntent.getBroadcast(CustomApplication.instance, ALARM_ID_MOVES, movesIntent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 19);

        Random r = new Random();
        int minute = r.nextInt(40-10)+10;
        calendar.set(Calendar.MINUTE, minute);

        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_HALF_DAY, movesAlarmIntent);
    }

    @Override
    protected void stopAction() {
        manager.cancel(alarmIntent);
        manager.cancel(movesAlarmIntent);
    }

    /**
     * Class receiving alarms from the alarm manager.
     */
    public static class AlarmReceiver extends BroadcastReceiver {

        /**
         * Required.
         */
        public AlarmReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Authenticator.registerOperation();

                    try {
                        if (EventTransfer.uploadEvents()) {
                            setLastSync();
                            Log.i(TAG, "EventTranser.uploadEvents successfull");
                        } else {
                            Log.i(TAG, "EventTranser.uploadEvents failed? (returned false)");
                        }
                    } catch (Exception e) {
                        Log.e(CustomApplication.instance.getPackageName(),
                                Log.getStackTraceString(e));
                    }

                    Authenticator.unRegisterOperation();
                }
            }).start();
        }
    }

    /**
     * Class receiving alarms from the alarm manager.
     */
    public static class MovesAlarmReceiver extends BroadcastReceiver {

        /**
         * Required.
         */
        public MovesAlarmReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (MovesHandler.uploadRawMoves()) {
                            Log.i(TAG, "Upload of MovesData Successful");
                        } else {
                            Log.i(TAG, "Upload of MovesData Failed");
                        }
                    } catch (Exception e) {
                        Log.e(CustomApplication.instance.getPackageName(),
                                Log.getStackTraceString(e));
                    }

                }
            }).start();
        }
    }
}
