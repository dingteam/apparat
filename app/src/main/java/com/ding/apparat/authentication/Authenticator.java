package com.ding.apparat.authentication;

import android.util.Log;

import com.ding.apparat.base.MonitorService;
import com.ding.apparat.http.GetRequest;
import com.ding.apparat.http.PostRequest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Locale;

/**
 * Handles authentication and related tasks.
 */
public class Authenticator {

    /**
     * The URL for registering new users.
     */
    private static final String REGISTER_URL = GetRequest.API_BASE + "register";

    /**
     * The URL for logging in.
     */
    private static final String LOGIN_URL = GetRequest.API_BASE + "login";

    /**
     * The URL for resetting password.
     */
    private static final String RESET_PASSWORD_URL = GetRequest.API_BASE + "resetPassword";

    /**
     * The URL for changing password.
     */
    private static final String CHANGE_PASSWORD_URL = GetRequest.API_BASE + "changePassword";

    /**
     * This lock is used to let running tasks finish before logging out.
     */
    private static final Object logoutLock = new Object();

    /**
     * Semaphore for amount of currently running operations. Used to avoid logging out before all
     * current actions have been completed.
     */
    private static int numberOfRunningOperations = 0;

    /**
     * Register that an operation prohibiting immediate logout is now started and running.
     */
    public static void registerOperation() {
        synchronized (logoutLock) {
            numberOfRunningOperations++;
        }
    }

    /**
     * Unregister an operation that prohibits logging out. Used when the operation has been
     * finished.
     */
    public static synchronized void unRegisterOperation() {
        synchronized (logoutLock) {
            numberOfRunningOperations--;
            if (numberOfRunningOperations <= 0) {
                logoutLock.notifyAll();
            }
        }
    }

    /**
     * Logs in a new user.
     *
     * @param email    The user email.
     * @param password The user password.
     * @return The http response code of the underlying network login attempt.
     * @throws IOException
     */
    public static int login(String email, String password) throws IOException {
        PostRequest connection = null;
        try {
            connection = new PostRequest(LOGIN_URL);
            connection.addParameter("email", email);
            connection.addParameter("password", password);

            if (connection.getResponseStatus() == HttpURLConnection.HTTP_OK) {
                User.set(email, connection.getResponse());
            }

            return connection.getResponseStatus();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Logs out the current user, gracefully exiting other currently running threads first (eg.
     * sync, poller).
     */
    public static void logout() throws InterruptedException {
        // Immediately request a stop to monitoring and synchronization.
        MonitorService.instance.stopRunners();

        // Wait for running operations to complete.
        if (numberOfRunningOperations > 0) {
            synchronized (logoutLock) {
                logoutLock.wait();
            }
        }

        // Logout
        User.set(null, null);

        // Stop service
        MonitorService.stop();
    }

    /**
     * Register a new user.
     *
     * @param email                   The user email.
     * @param password                The user password.
     * @param newPasswordConfirmation The confirmed user password.
     * @return The http response code of the underlying network register attempt.
     * @throws IOException
     */
    public static int register(String email, String password, String newPasswordConfirmation)
            throws IOException {
        PostRequest connection = null;
        try {
            connection = new PostRequest(REGISTER_URL);
            connection.addParameter("email", email);
            connection.addParameter("password", password);
            connection.addParameter("password_confirmation", newPasswordConfirmation);

            if (connection.getResponseStatus() == HttpURLConnection.HTTP_OK) {
                User.set(email, connection.getResponse());
            }

            return connection.getResponseStatus();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Resets a user password.
     *
     * @param email The email for which the password is to be reset.
     * @return The http response code of the underlying password reset attempt.
     * @throws IOException
     */
    public static int resetPassword(String email) throws IOException {
        PostRequest connection = null;
        try {
            connection = new PostRequest(RESET_PASSWORD_URL);
            connection.addParameter("email", email);
            Log.i("FLOWHUNT", "Locale language: " + Locale.getDefault().getLanguage());
            connection.addParameter("locale", Locale.getDefault().getLanguage());

            return connection.getResponseStatus();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Changes the user password.
     *
     * @param password                The old password.
     * @param newPassword             The new password.
     * @param newPasswordConfirmation The confirmed new password
     * @return The http response code of the underlying password change attempt.
     * @throws IOException
     */
    public static int changePassword(String password, String newPassword,
                                     String newPasswordConfirmation) throws IOException {
        PostRequest connection = null;
        try {
            connection = new PostRequest(CHANGE_PASSWORD_URL);
            connection.addParameter("email", User.getUser());
            connection.addParameter("login_uuid", User.getLoginUuid());
            connection.addParameter("password", password);
            connection.addParameter("new_password", newPassword);
            connection.addParameter("new_password_confirmation", newPasswordConfirmation);

            return connection.getResponseStatus();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
