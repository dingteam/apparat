package com.ding.apparat.authentication;

import com.ding.apparat.storage.PersistentGlobalValue;
import com.ding.apparat.storage.PersistentValue;

/**
 * Handles information about the currently logged in user.
 */
public class User {

    /**
     * Persistent object for logged in user.
     */
    private static final PersistentValue user = new PersistentGlobalValue("user");

    /**
     * Persistent object for login UUID.
     */
    private static final PersistentValue loginUuid = new PersistentGlobalValue("loginUuid");

    /**
     * @return The logged in user.
     */
    public static String getUser() {
        return user.getString();
    }

    /**
     * @return The login UUID for the current user.
     */
    public static String getLoginUuid() {
        return loginUuid.getString();
    }

    /**
     * @return If a user is currently logged in.
     */
    public static boolean loggedIn() {
        return getUser() != null && getLoginUuid() != null;
    }

    /**
     * Set the current user and login uuid.
     *
     * @param user      The user.
     * @param loginUuid The login uuid.
     */
    public static void set(String user, String loginUuid) {
        User.user.setString(user);
        User.loginUuid.setString(loginUuid);
    }
}
