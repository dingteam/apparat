package com.ding.apparat.http;

import com.ding.apparat.BuildConfig;
import com.ding.apparat.util.StringUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Wrapper for HttpURLConnection providing a simple interface for the usage required in this
 * specific application (GET requests).
 */
public class GetRequest {

    /**
     * The base URL for the API belonging to this application.
     */
    public static final String API_BASE = BuildConfig.API_BASE;

    /**
     * The underlying HttpURLConnection.
     */
    HttpURLConnection connection;

    /**
     * Initialize and configure the request for GET.
     *
     * @param path The URL as a string.
     * @throws IOException
     */
    public GetRequest(String path) throws IOException {
        URL url = new URL(path);
        connection = (HttpURLConnection) (url.openConnection());
    }

    /**
     * Establish the connection and get the response status.
     *
     * @return The status code for the response.
     * @throws IOException
     */
    public int getResponseStatus() throws IOException {
        //Log.d("Connecting to:", connection.getURL().toString());
        return connection.getResponseCode();
    }

    /**
     * Get the response body.
     *
     * @return The response body.
     * @throws IOException
     */
    public String getResponse() throws IOException {
        return StringUtils.getString(connection.getInputStream());
    }

    /**
     * Get the error response from the request.
     *
     * @return The error response.
     */
    public String getErrorResponse() {
        return StringUtils.getString(connection.getErrorStream());
    }

    /**
     * Adds a header to the request.
     *
     * @param key   The header key.
     * @param value The header value.
     */
    public void addHeader(String key, String value) {
        connection.setRequestProperty(key, value);
    }

    /**
     * Disconnect the underlying request.
     */
    public void disconnect() {
        connection.disconnect();
    }
}
