package com.ding.apparat.http;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * Wrapper for HttpURLConnection providing a simple interface for the usage required in this
 * specific application (POST requests with application/x-www-form-urlencoded).
 */
public class PostRequest extends GetRequest {

    /**
     * The connection character encoding.
     */
    private static final String CHARSET = "UTF-8";

    /**
     * The output stream to which the request body is written.
     */
    private OutputStream output;

    /**
     * If a parameter has been written to the output stream.
     */
    private boolean parameterAdded;

    /**
     * Initialize and configure the request for POST.
     *
     * @param path The URL as a string.
     * @throws IOException
     */
    public PostRequest(String path) throws IOException {
        super(path);
        parameterAdded = false;
        connection.setDoOutput(true);
        addHeader("Content-Type", "application/x-www-form-urlencoded");
        output = connection.getOutputStream();
    }

    /**
     * Add a POST parameter to the request, encoding it.
     *
     * @param key   The parameter key.
     * @param value The parameter value.
     * @throws IOException
     */
    public void addParameter(String key, String value) throws IOException {
        if (key == null || value == null) {
            return;
        }

        String query = "";

        if (parameterAdded) {
            query = "&";
        }

        query += String.format("%s=%s", URLEncoder.encode(key, CHARSET),
                URLEncoder.encode(value, CHARSET));

        output.write(query.getBytes(CHARSET));

        parameterAdded = true;
    }
}
