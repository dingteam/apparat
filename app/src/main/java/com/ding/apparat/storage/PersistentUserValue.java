package com.ding.apparat.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.ding.apparat.authentication.User;
import com.ding.apparat.base.CustomApplication;

/**
 * Persistent object for user data.
 */
public class PersistentUserValue extends PersistentValue {

    /**
     * Create a new instance of this class. Only used internally.
     *
     * @param key The object key.
     */
    public PersistentUserValue(String key) {
        super(key);
    }

    @Override
    protected SharedPreferences getBase() {
        return CustomApplication.instance
                .getSharedPreferences(User.getUser(), Context.MODE_PRIVATE);
    }
}
