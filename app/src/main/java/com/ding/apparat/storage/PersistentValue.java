package com.ding.apparat.storage;

import android.content.SharedPreferences;

/**
 * An object that is persistent on the device. Built upon SharedPreferences
 */
public abstract class PersistentValue {

    /**
     * The key for this object.
     */
    private final String key;

    /**
     * Create a new instance of this class. Only used internally.
     *
     * @param key The object key.
     */
    PersistentValue(String key) {
        // Prepend caller class name to key to avoid collisions.
        this.key = Thread.currentThread().getStackTrace()[4].getClassName() + "." + key;
    }

    /**
     * Used internally for retrieving a SharedPreferences to use as base for the object.
     *
     * @return The base SharedPreferences.
     */
    protected abstract SharedPreferences getBase();

    /**
     * Get the String this object represents.
     *
     * @return The String, or null if it does not exist.
     */
    public String getString() {
        return getBase().getString(key, null);
    }

    /**
     * Set the String this object represents.
     *
     * @param value The String to set.
     */
    public void setString(String value) {
        getBase().edit().putString(key, value).commit();
    }

    /**
     * Get the long this object represents.
     *
     * @return The long, or 0 if it does not exist.
     */
    public long getLong() {
        return getBase().getLong(key, 0);
    }

    /**
     * Set the long this object represents.
     *
     * @param value The long to set.
     */
    public void setLong(long value) {
        getBase().edit().putLong(key, value).commit();
    }

    /**
     * Get the float this object represents.
     *
     * @return The float, or 0 if it does not exist.
     */
    public float getFloat() {
        return getBase().getFloat(key, 0);
    }

    /**
     * Set the float this object represents.
     *
     * @param value The float to set.
     */
    public void setFloat(float value) {
        getBase().edit().putFloat(key, value).commit();
    }

    /**
     * Get the int this object represents.
     *
     * @return The int, or 0 if it does not exist.
     */
    public int getInt() {
        return getBase().getInt(key, 0);
    }

    /**
     * Set the int this object represents.
     *
     * @param value The int to set.
     */
    public void setInt(int value) {
        getBase().edit().putInt(key, value).commit();
    }

    /**
     * Get the boolean this object represents.
     *
     * @return The boolean, or false if it does not exist.
     */
    public boolean getBoolean() {
        return getBase().getBoolean(key, false);
    }

    /**
     * Set the boolean this object represents.
     *
     * @param value The boolean to set.
     */
    public void setBoolean(boolean value) {
        getBase().edit().putBoolean(key, value).commit();
    }
}
