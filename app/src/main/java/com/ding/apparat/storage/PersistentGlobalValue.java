package com.ding.apparat.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.ding.apparat.base.CustomApplication;

/**
 * Persistent object for global data.
 */
public class PersistentGlobalValue extends PersistentValue {

    /**
     * Create a new instance of this class. Only used internally.
     *
     * @param key The object key.
     */
    public PersistentGlobalValue(String key) {
        super(key);
    }

    @Override
    protected SharedPreferences getBase() {
        return CustomApplication.instance
                .getSharedPreferences(CustomApplication.instance.getPackageName(),
                        Context.MODE_PRIVATE);
    }
}
