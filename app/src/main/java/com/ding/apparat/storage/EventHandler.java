package com.ding.apparat.storage;

import android.content.Context;
import android.util.Log;

import com.ding.apparat.authentication.User;
import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.util.StringUtils;

import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Handles creating, storing and reading JSON from persistent storage.
 */
public class EventHandler {

    /**
     * The file output stream used for writing events.
     */
    private static FileOutputStream out;

    private static final String TAG = EventHandler.class.getSimpleName();

    /**
     * Write an Event to internal storage.
     *
     * @param event The event to be written.
     */
    public static synchronized void writeEvent(Event event) {
        try {
            boolean firstWrite = false;

            // If something has happened to the stream or file, recreate it.
            if (out == null || !userFileExists()) {
                if (!userFileExists()) {
                    firstWrite = true;
                }
                out = CustomApplication.instance
                        .openFileOutput(User.getUser(), Context.MODE_APPEND);
            }

            if (!firstWrite) {
                out.write(",".getBytes());
            }
            out.write((event.toJson()).getBytes());
        } catch (Exception e) {
            Log.e(CustomApplication.instance.getPackageName(), Log.getStackTraceString(e));
        }

        // Do NOT close the output stream, it will most likely be reused again shortly.
    }

    /**
     * If the currently logged in user's event file exists.
     *
     * @return If it exists.
     */
    private static boolean userFileExists() {
        return new File(CustomApplication.instance.getFilesDir(), User.getUser()).exists();
    }

    /**
     * @return A list of all stored events.
     */
    public static List<Event> readEvents()
            throws IOException, IllegalAccessException, JSONException {
        InputStream in = null;

        if (!userFileExists()) {
            return null;
        }

        try {
            in = CustomApplication.instance.openFileInput(User.getUser());
            return Event.fromJsonArray("[" + StringUtils.getString(in) + "]");
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * Deletes all stored events, freeing disk space.
     */
    public static void clearEvents() throws IOException {
        File file = new File(CustomApplication.instance.getFilesDir(), User.getUser());

        // Delete file
        //noinspection ResultOfMethodCallIgnored
        file.delete();

        // Close output stream if open
        if (out != null) {
            out.close();
        }
    }

    /**
     * Write the JSONArray to storage
     *
     * @param events The JSONArray.
     */
    public static void writeEvents(List<Event> events) {
        for (Event e : events) {
            writeEvent(e);
        }
    }
}
