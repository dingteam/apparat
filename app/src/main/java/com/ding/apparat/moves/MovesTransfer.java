package com.ding.apparat.moves;

import android.util.Log;

import com.ding.apparat.authentication.User;
import com.ding.apparat.http.GetRequest;
import com.ding.apparat.http.PostRequest;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Calendar;

public class MovesTransfer {
    private static final String TAG = MovesTransfer.class.getSimpleName();
    /**
     * The URL for uploading raw moves data to server.
     */
    private static final String UPLOAD_MOVES_URL = GetRequest.API_BASE + "uploadMoves";

    /**
     * The persistent object for the timestamp of the last day moves have been successfully called and transfered.
     */
    private static final PersistentValue lastMovesTimestamp = new PersistentUserValue("lastMovesTimestamp");

    /**
     * How many days back the Moves storyline API call should fetch data from (7 is max according to
     * documentation).
     */
    private static final int PAST_DAYS = 7;

    private static final String MOVES_URL_START = "https://api.moves-app.com/api/1.1/user/storyline/daily?pastDays=";
    private static final String MOVES_URL_FINAL = "&trackPoints=true";

    /**
     * The URL for making Moves API calls.
     */
    private static final String MOVES_URL = MOVES_URL_START + PAST_DAYS + MOVES_URL_FINAL;

    private static String generateMovesUrl(int days) {
        return (MOVES_URL_START + days + MOVES_URL_FINAL);
    }

    /**
     * Fetch the moves data from the API.
     *
     * @return The data.
     * @throws IOException
     * @throws JSONException
     */
    public static JSONArray fetchMovesData() throws IOException, JSONException {
        JSONObject token = MovesAuthenticator.getToken();

        if (token == null) {
            return null;
        }

        GetRequest connection = new GetRequest(MOVES_URL);
        connection.addHeader("Authorization", "Bearer " + token.getString("access_token"));

        if (connection.getResponseStatus() != HttpURLConnection.HTTP_OK) {
            throw new IOException();
        }

        return new JSONArray(connection.getResponse());
    }

    public static JSONArray fetchLimitedMovesData() throws IOException, JSONException {
        JSONObject token = MovesAuthenticator.getToken();

        if (token == null) {
            return null;
        }

        Log.i(TAG, "DOWNLOAD_MOVES_URL: " + generateMovesUrl(getMovesDays()));
        GetRequest connection = new GetRequest(generateMovesUrl(getMovesDays()));
        connection.addHeader("Authorization", "Bearer " + token.getString("access_token"));

        if (connection.getResponseStatus() != HttpURLConnection.HTTP_OK) {
            throw new IOException();
        }

        return new JSONArray(connection.getResponse());
    }


    /**
     * Post raw moves data to our API.
     *
     * @param movesArray
     * @return boolean whether the post was successful or not.
     * @throws IOException
     * @throws JSONException
     * @throws IllegalAccessException
     */
    public static boolean postMoves(JSONArray movesArray)
            throws IOException, JSONException, IllegalAccessException {

        Log.i(TAG, "postMoves");
        if (movesArray.length() < 1) {
            Log.i(TAG, "moves List empty");
            return false;
        }

        PostRequest connection = null;

        try {
            Log.d(TAG, "UPLOAD_MOVES_URL: " + UPLOAD_MOVES_URL);
            connection = new PostRequest(UPLOAD_MOVES_URL);
            connection.addParameter("data", movesArray.toString());
            connection.addParameter("email", User.getUser());
            connection.addParameter("login_uuid", User.getLoginUuid());
            Log.i(TAG, "email: " + User.getUser());
            Log.i(TAG, "login_uuid: " + User.getLoginUuid());

            if (connection.getResponseStatus() != HttpURLConnection.HTTP_OK) {
                Log.e(TAG, "Response status: " + connection.getResponseStatus());
                return false;
            }
            updateLastMovesTimestamp();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return true;
    }

    public static void updateLastMovesTimestamp(){
        Calendar currentCal = Calendar.getInstance();
        long currentInMillis = currentCal.getTimeInMillis();
        lastMovesTimestamp.setLong(currentInMillis);
    }

    private static int getMovesDays(){
        long lastTimestamp = lastMovesTimestamp.getLong();
        int movesDaysToFetch = 6;

        if(lastTimestamp != 0){
            Calendar currentCal = Calendar.getInstance();
            Calendar lastCal = Calendar.getInstance();

            lastCal.setTimeInMillis(lastTimestamp);

            int currentDay = currentCal.get(Calendar.DAY_OF_YEAR);
            int lastDay = lastCal.get(Calendar.DAY_OF_YEAR);

            movesDaysToFetch = currentDay - lastDay;
        }

        //Adding overlap margin to make sure nothing is lost if Moves update data just after we
        //last fetched
        movesDaysToFetch = movesDaysToFetch + 1;
        Log.i(TAG, "getMovesDays diff: " + movesDaysToFetch );
        return movesDaysToFetch;
    }



}
