package com.ding.apparat.moves;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.gui.menu.MenuActivity;
import com.ding.apparat.http.GetRequest;

/**
 * Dummy activity for handling Moves API related interactions.
 */
public class MovesActivity extends Activity {

    /**
     * Constants for onActivityResult.
     */
    private static final int INSTALL = 1;

    private static final int ENABLE = 2;

    private static final String TAG = MovesActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!MovesHandler.isMovesInstalled()) {
            // Redirect to play store install
            try {
                startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(
                                "market://details?id=" + MovesHandler.MOVES_PACKAGE_NAME)),
                        INSTALL);
            } catch (android.content.ActivityNotFoundException e) {
                startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(
                        "https://play.google.com/store/apps/details?id=" +
                                MovesHandler.MOVES_PACKAGE_NAME)), INSTALL);
            }
        } else if (MovesAuthenticator.getToken() == null) {
            // Request permission to use the Moves API.
            Uri movesUri = Uri.parse("moves://app/authorize?client_id=" + MovesAuthenticator.CLIENT_ID +
                    "&redirect_uri=" + GetRequest.API_BASE + "&scope=" +
                    MovesHandler.SCOPE);
            Log.i(TAG, movesUri.toString());

            startActivityForResult(new Intent(Intent.ACTION_VIEW,
                    movesUri), ENABLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG, "RequestCode: " + requestCode + ", resultCode: " + resultCode);
        Log.i(TAG, "Enable: " + ENABLE + ", Result_OK: " + RESULT_OK);


        if (requestCode == ENABLE) {
            if (resultCode == RESULT_OK) {
                Uri resultUri = data.getData();

                final String code = resultUri.getQueryParameter("code");

                Log.i(TAG, "Code: " + code);

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            MovesAuthenticator.receiveToken(code);
                        } catch (Exception e) {
                            Log.e(CustomApplication.instance.getPackageName(),
                                    Log.getStackTraceString(e));
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        redirectMenu();
                    }
                }.execute();

                return;
            } else if (data == null) {
                Intent i = getPackageManager()
                        .getLaunchIntentForPackage(MovesHandler.MOVES_PACKAGE_NAME);

                if (i != null) {
                    startActivity(i);
                }
            } else {
                Uri resultUri = data.getData();
                final String error = resultUri.getQueryParameter("error");
                Log.e(TAG, error);
            }
        }

        redirectMenu();
    }

    /**
     * Redirect the user back to the Menu.
     */
    private void redirectMenu() {
        startActivity(new Intent(this, MenuActivity.class));
        finish();
    }
}
