package com.ding.apparat.moves;

import android.util.Log;

import com.ding.apparat.http.GetRequest;
import com.ding.apparat.http.PostRequest;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Authenticate this application for using the Moves API.
 */
public class MovesAuthenticator {
    private static final String TAG = MovesAuthenticator.class.getSimpleName();

    /**
     * The Moves client ID for the application.
     */
    static final String CLIENT_ID = "hHTdJo3nDGvBClwpRN8Kxk8Dnb1Yi7Z8";

    /**
     * The API URL for receiving a Moves token.
     */
    private static final String RECEIVE_URL = GetRequest.API_BASE + "receiveMovesToken";

    /**
     * The API URL for refreshing the Moves token.
     */
    private static final String REFRESH_URL = GetRequest.API_BASE + "refreshMovesToken";

    /**
     * The API URL for validating the Moves token.
     */
    private static final String VALIDATION_URL =
            "https://api.moves-app.com/oauth/v1/tokeninfo?access_token=";

    /**
     * The currently stored token.
     */
    private static final PersistentValue token = new PersistentUserValue("token");

    /**
     * Receive an access token via the application API (used as a proxy).
     *
     * @param code The authorization code proving the end user has granted access to this
     *             application.
     * @throws IOException
     * @throws JSONException
     */
    public static void receiveToken(final String code) throws IOException, JSONException {
        PostRequest connection = null;

        try {
            connection = new PostRequest(RECEIVE_URL);
            connection.addParameter("code", code);

            if (connection.getResponseStatus() == HttpURLConnection.HTTP_OK) {
                Log.i(TAG, "Connection OK, saving token.");
                setToken(new JSONObject(connection.getResponse()));
            } else {
                Log.e(TAG, "Connection status: " + connection.getResponseStatus());
                Log.e(TAG, connection.getErrorResponse());
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Refresh the access token via the application API (used as a proxy).
     *
     * @return Whether or not the token could be refreshed
     * @throws IOException
     * @throws JSONException
     */
    private static boolean refreshToken() throws IOException, JSONException {
        JSONObject token = getToken();

        if (token == null) {
            return false;
        }

        PostRequest connection = null;

        try {
            connection = new PostRequest(REFRESH_URL);
            connection.addParameter("refresh_token", token.getString("refresh_token"));

            if (connection.getResponseStatus() == HttpURLConnection.HTTP_OK) {
                setToken(new JSONObject(connection.getResponse()));
                return true;
            } else {
                return false;
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Validate the token. If the token is not valid, try to refresh it.
     *
     * @return If the token is valid/could be made valid by refreshing.
     * @throws IOException
     * @throws JSONException
     */
    public static boolean validateToken() throws IOException, JSONException {
        Log.i(TAG, "validateToken");
        JSONObject token = getToken();

        if (token == null) {
            return false;
        }

        GetRequest connection = null;

        try {
            connection = new GetRequest(VALIDATION_URL + token.getString("access_token"));

            if (connection.getResponseStatus() == HttpURLConnection.HTTP_OK) {
                return true;
            } else {
                if (refreshToken()) {
                    return true;
                } else {
                    setToken(null);
                    return false;
                }
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Get the currently stored token.
     *
     * @return The currently stored token.
     */
    public static JSONObject getToken() {
        String json = token.getString();

        if (json == null) {
            return null;
        }

        try {
            return new JSONObject(json);
        } catch (JSONException e) {
            return null;
        }
    }

    /**
     * Set the currently stored token.
     *
     * @param object The token to be stored.
     */
    private static void setToken(JSONObject object) {
        if (object != null) {
            Log.i(TAG, object.toString());
            token.setString(object.toString());
        } else {
            token.setString(null);
        }
    }
}
