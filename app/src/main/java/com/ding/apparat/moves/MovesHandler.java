package com.ding.apparat.moves;

import android.content.pm.PackageManager;
import android.util.Log;

import com.ding.apparat.base.CustomApplication;
import com.ding.apparat.base.Event;
import com.ding.apparat.base.MovesTransport;
import com.ding.apparat.http.GetRequest;
import com.ding.apparat.storage.PersistentUserValue;
import com.ding.apparat.storage.PersistentValue;
import com.ding.apparat.util.StringUtils;
import com.ding.apparat.util.TimeUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Class for handling authorization and communication with the Moves API https://dev.moves-app.com/
 */
public class MovesHandler {
    private static final String TAG = MovesHandler.class.getSimpleName();

    /**
     * With what scope the Moves API will be used (one or both of location and activity, space
     * delimited).
     */
    static final String SCOPE = "location activity";

    /**
     * The Moves package name.
     */
    static final String MOVES_PACKAGE_NAME = "com.protogeo.moves";


    /**
     * The text for stationary activity.
     */
    private static final String STATIONARY = "stationary";


    /**
     * Determines if the Moves application is currently installed.
     *
     * @return If Moves is installed.
     */
    public static boolean isMovesInstalled() {
        PackageManager pm = CustomApplication.instance.getPackageManager();
        try {
            pm.getPackageInfo(MOVES_PACKAGE_NAME, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Uploads raw moves data to our API for pure storage.
     * @return
     */
    public static boolean uploadRawMoves(){
        try {
            JSONArray movesArray = MovesTransfer.fetchLimitedMovesData();
            if(movesArray != null) {
                Log.i(TAG, movesArray.toString());
                printRawMovesData(movesArray);
                if(MovesTransfer.postMoves(movesArray)){
                    Log.i(TAG, "Successfully uploaded moves-data");
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Helper method, printing the Raw Moves Data in a readable format.
     * @param jsonArr
     */
    private static void printRawMovesData(JSONArray jsonArr){
        try{
            for(int i = 0; i < jsonArr.length(); i++){
                JSONObject json = jsonArr.getJSONObject(i);
                JSONArray segmentArr = json.getJSONArray("segments");
                for(int j = 0; j < segmentArr.length(); j++){
                    JSONObject jsonSeg = segmentArr.getJSONObject(j);
                    Log.i(TAG, jsonSeg.toString());
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Appends Moves API data to JSONArray of events and removes the ones out of scope from this
     * array (the most recent events not covered by Moves yet). Returns a new JSONArray with those
     * still out of scope.
     *
     * @param events The array of events compatible (= matching time found) with Moves.
     * @return The events currently out of Moves time range.
     */
    public static List<Event> appendMoves(List<Event> events)
            throws JSONException, ParseException, IOException {
        Log.i(TAG, "appendMoves");
        if (MovesAuthenticator.getToken() == null) {
            return null;
        }

        StringUtils.PrintEventList(events);

        JSONArray movesData;

        // Fetch all Moves-data
        movesData = MovesTransfer.fetchMovesData();
        if (movesData == null) {
            return null;
        }

        // Parse data to a more comfortable format.
        movesData = parseMovesData(movesData);

        // Match with events
        List<Event> writeBack = new ArrayList<>();
        Collections.sort(events);
        Iterator<Event> iterator = events.iterator();

        int movesIndex = 0;
        int movesLength = movesData.length();

        while (iterator.hasNext()) {
            Event event = iterator.next();

            while (movesIndex < movesLength) {
                JSONObject movesDataElement = movesData.getJSONObject(movesIndex);

                long eventTime = TimeUtils.getLong(event.utc_start, TimeUtils.mysqlFormat);
                long movesStartTime = TimeUtils
                        .getLong(movesDataElement.getString("startTime"), TimeUtils.movesFormat);
                long movesEndTime = TimeUtils
                        .getLong(movesDataElement.getString("endTime"), TimeUtils.movesFormat);

                if (movesStartTime > eventTime) {
                    break;
                }
                // Post condition: movesStartTime <= eventTime

                if (eventTime <= movesEndTime) {
                    event.lat = movesDataElement.getString("lat");
                    event.lon = movesDataElement.getString("lon");
                    event.activity = movesDataElement.getString("activity");
                    break;
                }

                movesIndex++;
            }

            // Write rest to writeBack
            if (movesIndex >= movesLength) {
                writeBack.add(event);
                iterator.remove();
            }
        }

        StringUtils.PrintEventList(events);
        StringUtils.PrintEventList(writeBack);

        return writeBack;
    }


    /**
     * Parse the Moves data into a comfortable format used for this application.
     *
     * @param movesData The data to be parsed.
     * @return The parsed data.
     * @throws JSONException
     */
    private static JSONArray parseMovesData(JSONArray movesData) throws JSONException {
        JSONArray parsedData = new JSONArray();

        // Iterate all dates
        for (int i = 0; i < movesData.length(); ++i) {
            if (movesData.getJSONObject(i).isNull("segments")) {
                continue;
            }

            JSONArray segments = movesData.getJSONObject(i).getJSONArray("segments");

            // Iterate all segments
            for (int j = 0; j < segments.length(); ++j) {
                JSONObject segment = segments.getJSONObject(j);

                String segmentStartTime = segment.getString("startTime");
                String segmentEndTime = segment.getString("endTime");
                String startTime = segmentStartTime;
                String endTime = segmentEndTime;
                String lat = null;
                String lon = null;
                String activity;

                JSONObject place = segment.optJSONObject("place");
                if (place != null) {
                    JSONObject location = place.optJSONObject("location");
                    lat = location.getString("lat");
                    lon = location.getString("lon");
                }

                JSONArray activities = segment.optJSONArray("activities");
                if (activities != null) {
                    endTime = segmentStartTime; // Necessary for parsing.
                    // Iterate all activities
                    for (int k = 0; k < activities.length(); ++k) {
                        JSONObject activityItem = activities.getJSONObject(k);

                        activity = activityItem.getString("activity");

                        // If no time data is available, ignore this activity.
                        if (activityItem.isNull("startTime") || activityItem.isNull("endTime")) {
                            continue;
                        }

                        String newStartTime = activityItem.getString("startTime");
                        String newEndTime = activityItem.getString("endTime");

                        // Add previous gap in activity list
                        if (!newStartTime.equals(endTime)) {
                            parsedData.put(createJSONObject(endTime, newStartTime, lat, lon,
                                    STATIONARY));
                        }

                        startTime = newStartTime;
                        endTime = newEndTime;

                        if (!activityItem.isNull("trackPoints")) {
                            JSONArray trackPoints = activityItem.getJSONArray("trackPoints");

                            if (trackPoints.length() > 0) {
                                // Get central track point
                                JSONObject location =
                                        trackPoints.getJSONObject(trackPoints.length() / 2);

                                lat = location.getString("lat");
                                lon = location.getString("lon");
                            }
                        }

                        // Add moves activity to array
                        parsedData.put(createJSONObject(startTime, endTime, lat, lon, activity));
                    }

                    if (!endTime.equals(segmentEndTime)) {
                        parsedData.put(createJSONObject(endTime, segmentEndTime, lat, lon,
                                STATIONARY));
                    }
                } else if (place != null) {
                    // Add stationary activity for places lacking activities.
                    parsedData.put(createJSONObject(startTime, endTime, lat, lon, STATIONARY));
                }
            }
        }

        return parsedData;
    }

    /**
     * Helper method for parsing. Creates a new JSONObject representing a Moves "event".
     *
     * @param startTime The start time.
     * @param endTime   The end time.
     * @param lat       The latitude.
     * @param lon       The longitude.
     * @param activity  The activity.
     * @return The created JSONObject.
     * @throws JSONException
     */
    private static JSONObject createJSONObject(String startTime, String endTime, String lat,
                                               String lon, String activity) throws JSONException {
        JSONObject object = new JSONObject();
        object.putOpt("startTime", startTime);
        object.putOpt("endTime", endTime);
        object.putOpt("lat", lat);
        object.putOpt("lon", lon);
        object.putOpt("activity", activity);
        return object;
    }
}
