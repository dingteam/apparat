package com.ding.apparat.util;

import android.util.Log;

import com.ding.apparat.base.Event;

import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

/**
 * Helpers for managing strings and string resources.
 */
public class StringUtils {
    private static final String TAG = StringUtils.class.getSimpleName();

    /**
     * Helper method for reading whole input streams to strings.
     *
     * @param in The input stream to be transformed.
     * @return The corresponding string.
     */
    public static String getString(InputStream in) {
        Scanner s = new Scanner(in).useDelimiter("\\A");

        if (s.hasNext()) {
            return s.next();
        }

        return null;
    }

    public static void PrintEventList(List<Event> events) {
        Log.i(TAG, "PrintEventList");

        if (events == null) {
            Log.e(TAG, "EventsList is null");
            return;
        }

        Log.i(TAG, "Size: " + events.size());
        /*for(Event event : events){
            Log.i(TAG, event.toString());
        }*/
    }
}
