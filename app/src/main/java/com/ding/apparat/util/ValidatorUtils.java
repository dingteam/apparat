package com.ding.apparat.util;

import android.text.TextUtils;

/**
 * Helper class for various common validation operations.
 */
public class ValidatorUtils {

    /**
     * @param email The email to be validated.
     * @return If the email is valid.
     */
    public static boolean validateEmail(String email) {
        return !TextUtils.isEmpty(email) &&
                android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * @param first  The first string.
     * @param second The second string.
     * @return If the strings match.
     */
    public static boolean validateMatch(String first, String second) {
        return first.equals(second);
    }

    /**
     * General method for validating a string (check that it is not empty).
     *
     * @param s The string to be validated.
     * @return If the string is valid.
     */
    public static boolean validate(String s) {
        return s.length() > 0;
    }

    public static boolean validateTerms(boolean b){
        return b;
    }
}
