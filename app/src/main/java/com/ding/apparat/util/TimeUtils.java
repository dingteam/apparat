package com.ding.apparat.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Helpers for working with time.
 */
public class TimeUtils {

    /**
     * ISO 8601 date format for MYSQL DATETIME column.
     */
    public static final SimpleDateFormat mysqlFormat =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

    /**
     * ISO 8601 date format used by the Moves API.
     */
    public static final SimpleDateFormat movesFormat =
            new SimpleDateFormat("yyyyMMdd'T'HHmmssZ", Locale.US);

    /**
     * ISO 8601 date format for displaying in application.
     */
    private static final SimpleDateFormat displayFormat =
            new SimpleDateFormat("yyyy-MM-dd',' HH:mm", Locale.US);

    /**
     * Get current UTC time in ISO 8601 format.
     *
     * @return The UTC time string.
     */
    public static String getUtcString() {
        return getUtcString(now());
    }

    /**
     * Convert a long timestamp to UTC time in ISO 8601 format.
     *
     * @param time The timestamp.
     * @return The UTC time string.
     */
    public static String getUtcString(long time) {
        mysqlFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return mysqlFormat.format(new Date(time));
    }

    /**
     * Get current local time in display format.
     *
     * @return The local time string.
     */
    public static String getDisplayString() {
        return getDisplayString(now());
    }

    /**
     * Convert a long timestamp to local time in display format.
     *
     * @param time The timestamp.
     * @return The local time string.
     */
    public static String getDisplayString(long time) {
        return displayFormat.format(new Date(time));
    }

    /**
     * @return The current time measured in milliseconds since the epoch.
     */
    public static long now() {
        return new Date().getTime();
    }

    /**
     * Convert a time string in specified format to a long. If no time zone is specified in the
     * format, UTC is assumed.
     *
     * @param time A formatted string.
     * @param sdf  The time format for the string.
     * @return The time as a long, measured in milliseconds since the epoch.
     */
    public static long getLong(String time, SimpleDateFormat sdf) throws ParseException {
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.parse(time).getTime();
    }
}
