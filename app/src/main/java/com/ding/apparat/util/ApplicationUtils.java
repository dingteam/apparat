package com.ding.apparat.util;

import android.content.pm.PackageManager;

import com.ding.apparat.base.CustomApplication;

/**
 * Helpers for application monitoring.
 */
public class ApplicationUtils {

    /**
     * Package name for unknown packages.
     */
    private static final String PACKAGE_UNKNOWN = "unknown";

    /**
     * For the given package name, return the application name.
     *
     * @param packageName The package name.
     * @return The application name.
     */
    public static String getApplicationName(String packageName) {
        final PackageManager packageManager = CustomApplication.instance.getPackageManager();
        try {
            return packageManager.getApplicationLabel(
                    packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA))
                    .toString();
        } catch (PackageManager.NameNotFoundException e) {
            return PACKAGE_UNKNOWN;
        }
    }
}
